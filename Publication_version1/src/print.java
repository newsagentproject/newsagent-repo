import java.sql.*;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class print {
	
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static Scanner in = new Scanner(System.in);
	static Publication p=new Publication();
	
	public static void main(String[] args) {

		init_db(); 		
		int menuChoice = 0;
		final int STOP_APP = 5; 
		
		while (menuChoice != STOP_APP)
		{
			displayMainMenu(); 
			if (in.hasNextInt())
			{
				menuChoice = in.nextInt();
				switch (menuChoice)
				{
				case 1:
					displayAllPublication();
					break;
				case 2:
					displayByTitle();
					break;
				case 3:
					displayByID();
					break;
				case 4:
					addNewPublication(); 
					break;
				case 5:
					System.out.println("Program is closing...");
					cleanup_resources();  
					break;
				default:
					System.out.println("You entered an invalid choice, please try again...");	
				}
			}
			else
			{
				in.nextLine();
				System.out.println("You entered an invalid choice, please try again...");	
			}
		}		
	}

	public static void addNewPublication() 
	{
		for(int i=0; i<1;i++)
		{
		System.out.println("Enter Title of the item.");
		//String name = in.next();
		p.setTitle(in.next());
		/*if(in.next().length() < 30)
		{
			p.setTitle(in.next());	
			i++;
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Entered Characters exceeded..");
			i--;
		}*/
		System.out.println("Enter the Price of the Item.");
		//if(in.nextDouble().contains("[a-zA-Z]"))
		//String description = in.next();
		p.setPrice(in.nextDouble());
		}
		String str = "INSERT INTO  publication (PUB_ID, TITLE, PRICE) VALUES"
				+ "(null, '" + p.getTitle() + "', '" + p.getPrice() + "');";  
	    try
        { 
            stmt.executeUpdate(str); 
            System.out.println("Adding Success!");      
        } 
        catch (SQLException sqle) 
        { 
            System.out.println("Error: failed to display all products."); 
            System.out.println(sqle.getMessage());
            System.out.println(str);
        } 		
	}

	public static void displayByTitle() 
	{
		displayAllPublication(); 
		System.out.print("Enter the Publication Title");
		p.setTitle(in.next());		
		String str = "Select count(*) from publication where TITLE ='" + p.getTitle() +"';"; 
		try
		{ 
			rs= stmt.executeQuery(str);
			rs.next();
			int id = rs.getInt(1);
			if(id==0)
			{
				System.out.println("Invalid Product");
			}
			else
			{
				str = "Select * from publication where TITLE ='" + p.getTitle() +"';";  
				rs= stmt.executeQuery(str);
				while(rs.next())
				{
					p.setPub_id(rs.getInt(1));
					p.setTitle(rs.getString(2));
					p.setPrice(rs.getDouble(3));
	                System.out.printf("%-5d%20s%8.2f\n",p.getPub_id(), p.getTitle(), p.getPrice());
				}
			}
		}            
        catch (SQLException sqle) 
        { 
            System.out.println("Error: failed to display all products."); 
            System.out.println(sqle.getMessage());
            System.out.println(str);
   
		}
	}
	
	public static void displayByID() 
	{
		displayAllPublication(); 
		System.out.print("Enter the Publication ID to display");
		p.setPub_id(in.nextInt());	
		String str = "Select count(*) from publication where PUB_ID =" + p.getPub_id() +";"; 
		try
		{ 
			rs= stmt.executeQuery(str);
			rs.next();
			int id = rs.getInt(1);
			if(id==0)
			{
				System.out.println("Invalid Product");
			}
			else
			{
				str = "Select * from publication where PUB_ID =" + p.getPub_id() +";";  
				rs= stmt.executeQuery(str);
				while(rs.next())
				{ 
					p.setPub_id(rs.getInt(1));
					p.setTitle(rs.getString(2));
					p.setPrice(rs.getDouble(3));
	                System.out.printf("%-5d%20s%8.2f\n",p.getPub_id(), p.getTitle(), p.getPrice());
				}
			}
		}            
        catch (SQLException sqle) 
        { 
            System.out.println("Error: failed to display all products."); 
            System.out.println(sqle.getMessage());
            System.out.println(str);
   
		}
	}
	
	public static void displayAllPublication() 
	{
		String str = "Select * from publication;"; 				          
        try
        { 
            rs = stmt.executeQuery(str); 
            while (rs.next()) 
            { 
            	p.setPub_id(rs.getInt(1));
				p.setTitle(rs.getString(2));
				p.setPrice(rs.getDouble(3));
                System.out.printf("%-4d%20s%8.2f\n",p.getPub_id(), p.getTitle(), p.getPrice()); //printf is -=left handside d=integer 
            }    
        } 
        catch (SQLException sqle) 
        { 
            System.out.println("Error: failed to display all products."); 
            System.out.println(sqle.getMessage());
            System.out.println(str);
        } 	
	}
	public static void displayMainMenu()
	{
		System.out.println("Publication Menu\n");
		System.out.println("1: Display all Publication");
		System.out.println("2: Display a Publication by Title ");
		System.out.println("3:Display a Publication by ID ");
		System.out.println("4: Create a new Publication ");
		System.out.println("5: Exit application\n");
		System.out.print("Enter your choice: ");		
	}
	public static void cleanup_resources()
	{
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
	public static void init_db()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url="jdbc:mysql://localhost:3306/newsagent";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
}
