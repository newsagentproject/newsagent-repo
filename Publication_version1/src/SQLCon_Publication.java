import java.sql.*;
import net.proteanit.sql.DbUtils;
import javax.swing.JOptionPane;

public class SQLCon_Publication {
	
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	private static PreparedStatement prest;
	
	//static Scanner in = new Scanner(System.in);
	
	/*public static void addNewPartToProduct() 
	{
		System.out.println("Enter the product item that you want to add a part");			
		int enteredID= in.nextInt();
		String str = "Select count(*) from product where prod_id =" + enteredID +";"; 
		try  
		{ 
			rs= stmt.executeQuery(str);
			rs.next();
			int count = rs.getInt(1);
			if(count==0)
			{
				System.out.println("Invalid Product");
			}
			else
			{
				
				System.out.println("Enter the name of the Item.");
			String name = in.next();
			System.out.println("Enter the description of the Item.");
			String description = in.next();
			System.out.println("Enter the price of the Item.");
			double cost=in.nextDouble();
			str = "INSERT INTO part (prod_id,part_id, part_name, part_description, individual_price) VALUES"
						+ "(" +enteredID+",null, '"+ name + "', '" + description + "', " + cost + ");"; 
			
			stmt.executeUpdate(str); 
			System.out.println("Adding Successful!");
			}
	       }
	        catch (SQLException sqle) 
	        { 
	            System.out.println("Error: failed to display all products."); 
	            System.out.println(sqle.getMessage());
	            System.out.println(str);
	        } 
		}*/
	
	public static void addNewItem()
	{
		Publication p= new Publication();
		init_db();
		try 
		{
	        String sql = "INSERT INTO publication ( PUB_ID, TITLE, PRICE) VALUES (?,?,?)" ;
            prest = con.prepareStatement(sql);	
			prest.setInt(1,p.getPub_id());
			prest.setString(2,p.getTitle());
			prest.setDouble(3, p.getPrice());
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Insert Success!");
		}
		catch (SQLException err) {
			System.out.println(err.getMessage());
		}
	}
	
	public static void viewByName()
	{
		PublicationGUI p= new PublicationGUI();
		String byName = p.nameTextField.getText();
		String str = "Select * from publication where TITLE='" + byName + "\";";
		try
		{ 
			prest = con.prepareStatement(str);
			prest.setInt(1,Integer.parseInt(p.nameTextField.getText()));
			rs=prest.executeQuery();
			p.viewAllTable.setModel(DbUtils.resultSetToTableModel(rs));
			/*rs= stmt.executeQuery(str);
			rs.next();
			//int id = rs.getInt(1);
			if(byName.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"Please enter something");
			}
			else
			{
				str = "Select * from publication where TITLE='" + byName + "\";";  
				rs= stmt.executeQuery(str);
				while(rs.next())
				{				
					p.viewAllArea.setText(rs.getString(3));
				}
			}*/
		}
	    catch (SQLException sqle) 
	        { 
	            JOptionPane.showMessageDialog(null,"Error: failed to display all products."
	            		+ "\n" + sqle.getMessage() + "\n" + str); 
			}
	}
	
	public static void viewByID()
	{
		init_db();
		PublicationGUI p= new PublicationGUI();
		//String byID = p.idTextField.getText();
		String str = "Select * from publication where PUB_ID like ? ";
		try
		{ 
			prest = con.prepareStatement(str);
			prest.setInt(1,Integer.parseInt(p.idTextField.getText()));
			rs=prest.executeQuery();
			p.viewAllTable.setModel(DbUtils.resultSetToTableModel(rs));
			/*rs= stmt.executeQuery(str);
			rs.next();
			int id = rs.getInt(1);
			if(id==0)
			{
				JOptionPane.showMessageDialog(null,"Invalid ID Product");
			}
			else
			{
				str = "Select * from publication where PUB_ID=" + byID + ";";  
				rs= stmt.executeQuery(str);
				while(rs.next())
				{				
					p.viewAllTable.setText(rs.getString(3));
				}
			}*/
		}
	    catch (SQLException e) 
	        { 
	            JOptionPane.showMessageDialog(null,"Error: failed to display all products."
	            		+ "\n" + e.getMessage() + "\n" + str); 
	            e.printStackTrace();
			}
	}
	
	public static void viewAll()
	{
		PublicationGUI p= new PublicationGUI();
		init_db();
		String str = "Select * from publication";
		try
		{ 
			prest = con.prepareStatement(str);
			rs= prest.executeQuery(str);
			p.viewAllTable.setModel(DbUtils.resultSetToTableModel(rs));
		}
	    catch (SQLException e) 
	        { 
	            JOptionPane.showMessageDialog(null,"Error: failed to display all products."
	            		+ "\n" + e.getMessage() + "\n" + str); 
	            e.printStackTrace();
			}
	}
	
	public static void init_db()
	{
            try
            {
            	//jdbc:mysql://localhost:3306
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                String url="jdbc:mysql://localhost:3307/newsagent";
                con = DriverManager.getConnection(url, "root", "admin");
                stmt = con.createStatement(); 
                System.out.println("Database Connection Successful");
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null,"Error: Failed to connect to database\n"+e.getMessage());
            }
	}
	
}
