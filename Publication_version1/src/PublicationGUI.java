import java.awt.*;
import java.awt.event.*;
import java.beans.EventHandler;
import java.util.List;

import javax.swing.*;

//WORK ON FOR EACH BUTTON VIEW, VALIDATION, TESTING, SQL NOT IN THE TABLE


public class PublicationGUI extends JFrame implements ActionListener, WindowListener
{
	String Title;
	int Id;
	double price;
	Publication p= new Publication();
	
	//private JFrame frame1;
	private JPanel panel1= new JPanel();
	String [] searchBy= {"Add new Publication Item", "Search by Publication ID", "Search by Publication Name", "View All"};
	
	private JScrollPane scroll = new javax.swing.JScrollPane();
	private JLabel addNameLabel= new JLabel("Enter Publication Name");		public JTextField addNameTextField = new JTextField();
	private JLabel addIdLabel= new JLabel("Enter Publication ID: ");		public JTextField addIdTextField = new JTextField();
	private JLabel addPriceLabel= new JLabel("Enter Publication Price: ");  public JTextField addPriceTextField = new JTextField();
	private JButton addItemButton = new JButton("Add");
	
	private JLabel searchLabel= new JLabel("Choose: ");						private JComboBox comboBox1= new JComboBox(searchBy);
    public JTable viewAllTable= new JTable();
	private JLabel idLabel= new JLabel("Enter Publication ID: ");			public JTextField idTextField = new JTextField();
	private JLabel nameLabel= new JLabel("Enter Publication Name: ");		public JTextField nameTextField = new JTextField();
	public JTextArea viewAllArea= new JTextArea();
	private JButton viewByIDButton = new JButton("Enter");
	private JButton viewByNameButton = new JButton("Enter");
	private JButton viewAllButton = new JButton("View All");
	
	//private boolean nameSelection="false";
	//private boolean nameSelection="false";
	
	public PublicationGUI()
	{
		setLayout(null);
		setSize(400,300);
		setTitle("Publication");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		addIdLabel.setBounds(40,80,150,20);			addIdTextField.setBounds(200,80,180,20);
		addNameLabel.setBounds(40,120,150,20);      addNameTextField.setBounds(200,120,180,20);
		addPriceLabel.setBounds(40,160,150,20);     addPriceTextField.setBounds(200,160,180,20);
													addItemButton.setBounds(300,230,80,20);
		
		searchLabel.setBounds(40,40,150,20);	comboBox1.setBounds(200,40,180,20);
		idLabel.setBounds(40,80,150,20);		idTextField.setBounds(200,80,180,20);
		nameLabel.setBounds(40,80,150,20);		nameTextField.setBounds(200,80,180,20);
        viewAllTable.setBounds(40,120,340,100);

        //viewAllTable.setFont(new java.awt.Font("Arial", 0, 12));
        viewAllTable.setModel(new javax.swing.table.DefaultTableModel(
        		new Object [][] 
    				{
	    				{null, null, null},
	    				{null, null, null},
	    				{null, null, null},
	    				{null, null, null},
	    				{null, null, null}
    				},
    				new String [] {"PUB_ID", "TITLE", "PRICE"}
        		) 
        	{
        	Class[] types = new Class [] 
    			{
        			java.lang.Integer.class, java.lang.String.class, java.lang.String.class
    			};
	        public Class getColumnClass(int columnIndex) {
	        return types [columnIndex];
	        }
        	});

        	scroll.setViewportView(viewAllTable);
		//viewAllArea.setBounds(40,120,340,100);
												viewByIDButton.setBounds(300,230,80,20);
												viewByNameButton.setBounds(300,230,80,20);
												viewAllButton.setBounds(300,230,80,20);
		
		/*GroupLayout layout= new GroupLayout(panel1);
		 layout.setHorizontalGroup( layout.createSequentialGroup()
                 .addGroup( layout.createParallelGroup( GroupLayout.Alignment.LEADING )
                                    .addComponent( searchLabel)
                                    .addComponent( idLabel )
                                    .addComponent( nameLabel ) 
                                    	.addGroup(layout.createSequentialGroup()
                                    	.addGroup(layout.createParallelGroup(
                                    	GroupLayout.Alignment.LEADING)
                                    	.addComponent( viewAllArea )))
                                    .addComponent( viewButton )));
		 
		 layout.setVerticalGroup(layout.createSequentialGroup()
				 .addComponent( comboBox1 )
                 .addComponent( idTextField )
                 .addComponent( nameTextField ) 
                 .addComponent( viewAllButton));*/                              
	}
	
	public void init()
	{
		comboBox1.addActionListener(this);
		viewByIDButton.addActionListener(this);
		viewByNameButton.addActionListener(this);
		viewAllButton.addActionListener(this);
		//comboBox1.setPreferredSize(new Dimension(1,25));
		addWindowListener(this);
		this.add(searchLabel);		this.add(comboBox1);
		
		this.add(addIdLabel);		this.add(addIdTextField);
		this.add(addNameLabel);		this.add(addNameTextField);
		this.add(addPriceLabel);	this.add(addPriceTextField);
		this.add(addItemButton);
		
		//searchLabel.setBounds()
		this.add(idLabel);			this.add(idTextField);
		this.add(nameLabel);		this.add(nameTextField);
                this.add(viewAllTable);
		//this.add(viewAllArea);
		this.add(viewByIDButton);	
		this.add(viewByNameButton);	
		/*viewByNameButton.setAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        label.setText("Accepted");
		    }
		});*/
		this.add(viewAllButton);
		this.setVisible(true);
	}
	 
	@Override
	public void windowActivated(WindowEvent e)
	{
		searchLabel.setVisible(true);		comboBox1.setVisible(true);
		addIdLabel.setVisible(false);		addIdTextField.setVisible(false);
		addNameLabel.setVisible(false);		addNameTextField.setVisible(false);
		addPriceLabel.setVisible(false);	addPriceTextField.setVisible(false);
											addItemButton.setVisible(false);
		idLabel.setVisible(false);			idTextField.setVisible(false);
		nameLabel.setVisible(false);		nameTextField.setVisible(false);
                viewAllTable.setVisible(false);
		//viewAllArea.setVisible(false);
		viewByIDButton.setVisible(false);	viewByNameButton.setVisible(false);	viewAllButton.setVisible(false);
	}
	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
	}	
	
	@Override
	public void actionPerformed (ActionEvent e)
	{	
            SQLCon_Publication pcon= new SQLCon_Publication();
            Publication p= new Publication();
            if (e.getSource() == comboBox1)
            {               
                if (comboBox1.getSelectedIndex()==0)
                {
                        addNameLabel.setVisible(true);	addNameTextField.setVisible(true);
                        addIdLabel.setVisible(true);	addIdTextField.setVisible(true);
                        addPriceLabel.setVisible(true);	addPriceTextField.setVisible(true);
                        addItemButton.setVisible(true);

                        nameLabel.setVisible(false);	nameTextField.setVisible(false);
                                                                                        viewByNameButton.setVisible(false);
                        idLabel.setVisible(false);		idTextField.setVisible(false);
                        viewAllTable.setVisible(false);
                        //viewAllArea.setVisible(false);
                                                                                        viewByIDButton.setVisible(false);
                }

                else if (comboBox1.getSelectedIndex()==1)
                {
                        addNameLabel.setVisible(false);	addNameTextField.setVisible(false);
                        addIdLabel.setVisible(false);	addIdTextField.setVisible(false);
                        addPriceLabel.setVisible(false);	addPriceTextField.setVisible(false);
                        addItemButton.setVisible(false);

                        nameLabel.setVisible(false);	nameTextField.setVisible(false);
                                                        viewByNameButton.setVisible(false);
                        idLabel.setVisible(true);		idTextField.setVisible(true);
                        viewAllTable.setVisible(true);
                        //viewAllArea.setVisible(true);
                                                        viewByIDButton.setVisible(true);
                }

                else if (comboBox1.getSelectedIndex()==2)
                {
                        addNameLabel.setVisible(false);	addNameTextField.setVisible(false);
                        addIdLabel.setVisible(false);	addIdTextField.setVisible(false);
                        addPriceLabel.setVisible(false);	addPriceTextField.setVisible(false);
                        addItemButton.setVisible(false);

                        idLabel.setVisible(false);		idTextField.setVisible(false);
                                                                                        viewByIDButton.setVisible(false);
                        nameLabel.setVisible(true); 	nameTextField.setVisible(true);
                        viewAllTable.setVisible(true);
                        //viewAllArea.setVisible(true);
                        viewByNameButton.setVisible(true);
                }

                else if (comboBox1.getSelectedIndex()==3)
                {
                        addNameLabel.setVisible(false);	addNameTextField.setVisible(false);
                        addIdLabel.setVisible(false);	addIdTextField.setVisible(false);
                        addPriceLabel.setVisible(false);	addPriceTextField.setVisible(false);
                        addItemButton.setVisible(false);

                        idLabel.setVisible(false);		idTextField.setVisible(false);
                                                                                        idTextField.setVisible(false);
                        nameLabel.setVisible(false);	nameTextField.setVisible(false);
                                                                                        viewByNameButton.setVisible(false);
                        viewAllTable.setVisible(true);
                        //viewAllArea.setVisible(true);
                        viewAllButton.setVisible(true);
                }
            }
            
            else if (e.getSource() == addItemButton)
            {
            	p.setTitle(addNameTextField.getText());
            	//p.setPub_id(Integer.parseInt(idTextField.getText()));
            	p.setPrice(Double.parseDouble(addPriceTextField.getText()));
            	pcon.addNewItem();
            }

            else if (e.getSource() == viewByIDButton)
            {
            	p.setPub_id(Integer.parseInt(idTextField.getText()));
               //pcon.viewByID();
            }

            else if (e.getSource() == viewByNameButton)
            {
                pcon.viewByName();
            }

            else if (e.getSource() == viewAllButton)
            {
            	
                pcon.viewAll();
            }     
	}
	
	/*public static void main(String [] args)
	{
		new PublicationGUI().init();
		SwingUtilities.invokeLater(f);
		f.setVisible(true);
	}*/
}
