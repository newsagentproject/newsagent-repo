import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {

	//test number:0001
	//test objective:valid Customer Id
	//input(s) : 1001
	//expected output(s):true
	@Test
	public void testCustomer0001() throws InvalidCustomerExceptionHandler
	{
		Customer c = new Customer();
		assertEquals(true,c.validateCustomer(1001, "John", "Doe", "N93 5h56", "7 Street", "apt rd", "Tullamore", "Offaly"));
	}
	//test number:0002
		//test objective:invalid Customer Id
		//input(s) : 999
		//expected output(s):false
		@Test
		public void testCustomer0002() throws InvalidCustomerExceptionHandler
		{
			Customer c = new Customer();
			assertEquals(false,c.validateCustomer(999, "John", "Doe", "N93 5h56", "7 Street", "apt rd", "Tullamore", "Offaly"));
		}
		//test number:0003
				//test objective:valid Customer Id
				//input(s) : max integer
				//expected output(s):true
				@Test
				public void testCustomer0003() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(true,c.validateCustomer((Integer.MAX_VALUE), "John", "Doe", "N93 5h56", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0004
				//test objective:invalid Customer Id
				//input(s) : max int +1
				//expected output(s):false
				@Test
				public void testCustomer0004() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(false,c.validateCustomer((Integer.MAX_VALUE+1), "John", "Doe", "N93 5h56", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}

				//test number:0005
				//test objective:valid Customer Id
				//input(s) : max int -1
				//expected output(s):true
				@Test
				public void testCustomer0005() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(true,c.validateCustomer((Integer.MAX_VALUE-1), "John", "Doe", "N93 5h56", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0006
				//test objective:invalid Customer firstname
				//input(s) : abcdefghijklmnopqrstuvwxyz
				//expected output(s):false
				@Test
				public void testCustomer0006() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(false,c.validateCustomer(1001, "abcdefghijklmnopqrstuvwxyz", "Doe", "N93 5h56", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0007
				//test objective:valid Customer firstname
				//input(s) : john
				//expected output(s):true
				@Test
				public void testCustomer0007() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(true,c.validateCustomer(1001, "john", "Doe", "N93 5h56", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0008
				//test objective:invalid Customer lastname
				//input(s) : abcdefghijklmnopqrstuvwxyz
				//expected output(s):false
				@Test
				public void testCustomer0008() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(false,c.validateCustomer(1001, "john", "abcdefghijklmnopqrstuvwxyz", "N93 5h56", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0009
				//test objective:valid Customer lastname
				//input(s) : doe
				//expected output(s):true
				@Test
				public void testCustomer0009() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(true,c.validateCustomer(1001, "john", "doe", "N93 5h56", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0010
				//test objective:invalid eircode
				//input(s) : 123456789
				//expected output(s):false
				@Test
				public void testCustomer0010() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(false,c.validateCustomer(1001, "john", "doe", "123456789", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0011
				//test objective:valid eircode
				//input(s) : 12345678
				//expected output(s):true
				@Test
				public void testCustomer0011() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(true,c.validateCustomer(1001, "john", "doe", "12345678", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0012
				//test objective:invalid address 1
				//input(s) : abcdefghijklmnopqrstuvwxyz
				//expected output(s):false
				@Test
				public void testCustomer0012() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(false,c.validateCustomer(1001, "john", "doe", "12345678", "abcdefghijklmnopqrstuvwxyz", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0013
				//test objective:valid address1
				//input(s) : 7 street
				//expected output(s):true
				@Test
				public void testCustomer0013() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(true,c.validateCustomer(1001, "john", "doe", "12345678", "7 Street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0014
				//test objective:invalid address 2
				//input(s) : abcdefghijklmnopqrstuvwxyz
				//expected output(s):false
				@Test
				public void testCustomer0014() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(false,c.validateCustomer(1001, "john", "doe", "12345678", "7 street", "abcdefghijklmnopqrstuvwxyz", "Tullamore", "Offaly"));
				}
				//test number:0015
				//test objective:valid address 2
				//input(s) : 7 street
				//expected output(s):true
				@Test
				public void testCustomer0015() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(true,c.validateCustomer(1001, "john", "doe", "12345678", "7 street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0016
				//test objective:invalid town
				//input(s) : abcdefghijklmnopqrstuvwxyz
				//expected output(s):false
				@Test
				public void testCustomer0016() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(false,c.validateCustomer(1001, "john", "doe", "12345678", "7 street", "apt rd", "abcdefghijklmnopqrstuvwxyz", "Offaly"));
				}
				//test number:0017
				//test objective:valid town
				//input(s) : Tullamore
				//expected output(s):true
				@Test
				public void testCustomer0017() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(true,c.validateCustomer(1001, "john", "doe", "12345678", "7 street", "apt rd", "Tullamore", "Offaly"));
				}
				//test number:0018
				//test objective:valid County
				//input(s) : abcdefghijklmnopqrstuvwxyz
				//expected output(s):false
				@Test
				public void testCustomer0018() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(false,c.validateCustomer(1001, "john", "doe", "12345678", "7 street", "apt rd", "Tullamore", "abcdefghijklmnopqrstuvwxyz"));
				}
				//test number:0019
				//test objective:valid County
				//input(s) : Offaly
				//expected output(s):true
				@Test
				public void testCustomer0019() throws InvalidCustomerExceptionHandler
				{
					Customer c = new Customer();
					assertEquals(true,c.validateCustomer(1001, "john", "doe", "12345678", "7 street", "apt rd", "Tullamore", "Offaly"));
				}
}
