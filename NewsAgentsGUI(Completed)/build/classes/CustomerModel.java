import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class CustomerModel {
	
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	private static PreparedStatement prest=null;
	
       // DBConnection db= new DBConnection();
        
//	public PublicationDAO()
//	{
//		con=DBConnection.getConnection();
//	}

        public Customer updateCustomer(Customer c)
        {	
            init_db();
		try 
		{
	        String sql = "UPDATE customer SET  Firstname= ?, Lastname = ?, Eir_code=?, Address1=?, Address2=?, Town=?, County=? WHERE cus_id = ?" ;
                prest = con.prepareStatement(sql);	
			//prest.setInt(1,pub.getPub_id());
			prest.setString(1,c.getFirstName());
			prest.setString(2, c.getLastName());
                        prest.setInt(3,c.getEir_Code());
			prest.setString(4, c.getAddress1());
                        prest.setString(5,c.getAddress2());
			prest.setString(6, c.getTown());
                        prest.setString(7, c.getCounty());
                        prest.setInt(8, c.getCus_id());
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Update Success!");
		}
		catch (SQLException err) 
                {
			System.out.println(err.getMessage());
		}
                return c;
	}
	public void addCustomer(Customer cus)
	{	
		init_db();
		try 
		{
	        String sql = "INSERT INTO customer ( cus_id, Firstname, Lastname, Eir_code, Address1, Address2, Town,County) VALUES (null,?,?,?,?,?,?,?)" ;
            prest = con.prepareStatement(sql);	
			//prest.setInt(1,pub.getPub_id());
			prest.setString(1,cus.getFirstName());
			prest.setString(2, cus.getLastName());
                        prest.setInt(3,cus.getEir_Code());
			prest.setString(4, cus.getAddress1());
                        prest.setString(5,cus.getAddress2());
			prest.setString(6, cus.getTown());
                        prest.setString(7, cus.getCounty());
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Insert Success!");
		}
		catch (SQLException err) {
			System.out.println(err.getMessage());
		}
	}
	
	public static void deleteCustomer(int id)
	{	
		init_db();
		
		try 
		{
	        String sql = "DELETE from customer WHERE cus_id like ?" ;
                prest = con.prepareStatement(sql);	
			prest.setInt(1,id);
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Deletion Success!");
		}
		catch (SQLException err) {
			System.out.println(err.getMessage());
		}
	}
	
//	public Publication viewByName(String title)
//	{
//		init_db();
//		Publication p= new Publication();
//		String str = "Select * from Publication where TITLE like ? ";
//		try
//		{ 
//			prest = con.prepareStatement(str);
//			prest.setString(1,title);
//			rs=prest.executeQuery();
//			while(rs.next())
//			{
//				p.setPub_id(rs.getInt("PUB_ID"));
//				p.setTitle(rs.getString("TITLE"));
//				p.setPrice(rs.getDouble("PRICE"));
//			}
//		}
//		catch (SQLException e) 
//        { 
//            e.printStackTrace();
//		}
//		return p;
//	}
	
	public Customer viewByID(int id)
	{
            init_db();
           Customer c=new Customer();
            String str = "Select * from customer where cus_id like ?";
            try
            { 
                prest =con.prepareStatement(str);
                prest.setInt(1,id);
                rs=prest.executeQuery();
                while(rs.next())
                {
                      c.setCus_id(rs.getInt("cus_id"));
                                c.setFirstName(rs.getString("Firstname"));
                                c.setLastName(rs.getString("Lastname"));
                                c.setEir_Code(rs.getInt("Eir_code"));
                                c.setAddress1(rs.getString("Address1"));
                                c.setAddress2(rs.getString("Address2"));
                                c.setTown(rs.getString("Town"));
                                c.setCounty(rs.getString("County"));
                }
//                DBConnection.con.close();
                //System.out.println("PubID: " + p );
            }
	    catch (SQLException e) 
	        { 
	            e.printStackTrace();
			}
		return c;
	}
	
	
	
	public List<Customer> viewAll()
	{
		init_db();
		List <Customer> cus= new ArrayList<Customer>();
		String str = "Select * from customer";
		try
		{ 
			stmt= con.createStatement();
			rs= stmt.executeQuery(str);
			while (rs.next())
			{
				Customer c=new Customer();
				c.setCus_id(rs.getInt("cus_id"));
                                c.setFirstName(rs.getString("Firstname"));
                                c.setLastName(rs.getString("Lastname"));
                                c.setEir_Code(rs.getInt("Eir_code"));
                                c.setAddress1(rs.getString("Address1"));
                                c.setAddress2(rs.getString("Address2"));
                                c.setTown(rs.getString("Town"));
                                c.setCounty(rs.getString("County"));
                                
			}
		}
	    catch (SQLException e) 
        { 
            JOptionPane.showMessageDialog(null,"Error: failed to display all products."
            		+ "\n" + e.getMessage() + "\n" + str); 
            e.printStackTrace();
		}
		return cus;
	}
	
	public static void init_db()
	{
        try
        {
        	//jdbc:mysql://localhost:3306
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url="jdbc:mysql://localhost:3307/newsagent";
            con = DriverManager.getConnection(url, "root", "admin");
            stmt = con.createStatement(); 
            System.out.println("Database Connection Successful");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Error: Failed to connect to database\n"+e.getMessage());
        }          
	}
	
}