import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class DeliveryModel {
	
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	private static PreparedStatement prest=null;
	
       // DBConnection db= new DBConnection();
        
//	public PublicationDAO()
//	{
//		con=DBConnection.getConnection();
//	}

        public Delivery updateDelivery(Delivery d)
        {	
            init_db();
		try 
		{
	        String sql = "UPDATE delivery SET  delivery_id= ?, price = ? bill_id=? staff_id=? day=? month=? year=? lastAssigned = ? WHERE delivery_id = ?" ;
                prest = con.prepareStatement(sql);	
			//prest.setInt(1,pub.getPub_id());
			prest.setInt(1,d.getDelivery_id());
			prest.setDouble(2, d.getPrice());
                        prest.setInt(3,d.getBill_id());
			prest.setInt(4, d.getStaff_id());
                        prest.setString(5,d.getDate());
                        prest.setString(6, d.getLastAssigned());
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Update Success!");
		}
		catch (SQLException err) 
                {
			System.out.println(err.getMessage());
		}
                return d;
	}
	public void addDelivery(Delivery del)
	{	
		init_db();
		try 
		{
	        String sql = "INSERT INTO delivery ( delivery_id, price, bill_id,staff_id,day,month,year) VALUES (null,?,?,?,?,?,?,?)" ;
            prest = con.prepareStatement(sql);	
			//prest.setInt(1,pub.getPub_id());
			prest.setInt(1,del.getDelivery_id());
			prest.setDouble(2, del.getPrice());
                        prest.setInt(3,del.getBill_id());
			prest.setInt(4, del.getStaff_id());
                        prest.setString(5,del.getDate());
                        prest.setString(6,del.getLastAssigned());
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Insert Success!");
		}
		catch (SQLException err) {
			System.out.println(err.getMessage());
		}
	}
	
	public static void deleteDelivery(int id)
	{	
		init_db();
		
		try 
		{
	        String sql = "DELETE from delivery WHERE delivery_id like ?" ;
                prest = con.prepareStatement(sql);	
			prest.setInt(1,id);
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Deletion Success!");
		}
		catch (SQLException err) {
			System.out.println(err.getMessage());
		}
	}
	
//	public Publication viewByName(String title)
//	{
//		init_db();
//		Publication p= new Publication();
//		String str = "Select * from Publication where TITLE like ? ";
//		try
//		{ 
//			prest = con.prepareStatement(str);
//			prest.setString(1,title);
//			rs=prest.executeQuery();
//			while(rs.next())
//			{
//				p.setPub_id(rs.getInt("PUB_ID"));
//				p.setTitle(rs.getString("TITLE"));
//				p.setPrice(rs.getDouble("PRICE"));
//			}
//		}
//		catch (SQLException e) 
//        { 
//            e.printStackTrace();
//		}
//		return p;
//	}
	
	public Delivery viewByID(int id)
	{
            init_db();
           Delivery d=new Delivery();
            String str = "Select * from delivery where Delivery_ID like ?";
            try
            { 
                prest =con.prepareStatement(str);
                prest.setInt(1,id);
                rs=prest.executeQuery();
                while(rs.next())
                {
                      d.setPrice(rs.getDouble("price"));
                      d.setBill_id(rs.getInt("bill_id"));
                      d.setStaff_id(rs.getInt("staff_id"));
                      d.setLastAssigned(rs.getString("DATE"));
                }
//                DBConnection.con.close();
                //System.out.println("PubID: " + p );
            }
	    catch (SQLException e) 
	        { 
	            e.printStackTrace();
			}
		return d;
	}
	
	
	
	public List<Delivery> viewAll()
	{
		init_db();
		List <Delivery> cus= new ArrayList<Delivery>();
		String str = "Select * from delivery";
		try
		{ 
			stmt= con.createStatement();
			rs= stmt.executeQuery(str);
			while (rs.next())
			{
				Delivery d=new Delivery();
				d.setBill_id(rs.getInt("bill_id"));
                                d.setStaff_id(rs.getInt("staff_id"));
                                d.setPrice(rs.getDouble("price"));
                                d.setLastAssigned(rs.getString("lastAssigned"));
                                //d.(rs.getString("Eir_code"));               
			}
		}
	    catch (SQLException e) 
        { 
            JOptionPane.showMessageDialog(null,"Error: failed to display all products."
            		+ "\n" + e.getMessage() + "\n" + str); 
            e.printStackTrace();
		}
		return cus;
	}
	
	public static void init_db()
	{
        try
        {
        	//jdbc:mysql://localhost:3306
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url="jdbc:mysql://localhost:3307/newsagent";
            con = DriverManager.getConnection(url, "root", "admin");
            stmt = con.createStatement(); 
            System.out.println("Database Connection Successful");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Error: Failed to connect to database\n"+e.getMessage());
        }          
	}
	
}