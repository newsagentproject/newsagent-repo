import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class PublicationDAO {
	
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	private static PreparedStatement prest=null;
        private static PreparedStatement prest2=null;
        private static PreparedStatement prest3=null;
	
       // DBConnection db= new DBConnection();
        
//	public PublicationDAO()
//	{
//		con=DBConnection.getConnection();
//	}

        public Publication updatePublication(Publication p)//done
	{	
            init_db();
            //Publication p= new Publication();
		try 
		{
	        String sql = "UPDATE Publication SET TITLE = ?, PRICE = ? WHERE PUB_ID = ?" ;
                prest = con.prepareStatement(sql);	
			//prest.setInt(1,pub.getPub_id());
			prest.setString(1,p.getTitle());
			prest.setDouble(2, p.getPrice());
                        prest.setInt(3,p.getPub_id());
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Update Success!");
		}
		catch (SQLException err) 
                {
			System.out.println(err.getMessage());
		}
                return p;
	}
	public void addPublication(Publication pub)
	{	
		init_db();
		try 
		{
	        String sql = "INSERT INTO Publication ( PUB_ID, TITLE, PRICE) VALUES (null,?,?)" ;
            prest = con.prepareStatement(sql);	
			//prest.setInt(1,pub.getPub_id());
			prest.setString(1,pub.getTitle());
			prest.setDouble(2, pub.getPrice());
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Insert Success!");
		}
		catch (SQLException err) {
			System.out.println(err.getMessage());
		}
	}
	
	public static void deletePublication(int id)
	{	
		init_db();
		
		try 
		{
	        String sql = "DELETE from Publication WHERE PUB_ID=" + id ;
                String sql2= "DELETE from orders WHERE PUB_ID=" + id ;
                String sql3= "DELETE from contain WHERE PUB_ID=" + id ;
                prest2 = con.prepareStatement(sql2);
                prest3 = con.prepareStatement(sql3);
                prest = con.prepareStatement(sql);
			//prest.setInt(1,id);
                        prest2.executeUpdate();
                        prest3.executeUpdate();
                        prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Deletion Success!");
		}
		catch (SQLException err) {
			System.out.println(err.getMessage());
		}
	}
	
//	public Publication viewByName(String title)
//	{
//		init_db();
//		Publication p= new Publication();
//		String str = "Select * from Publication where TITLE like ? ";
//		try
//		{ 
//			prest = con.prepareStatement(str);
//			prest.setString(1,title);
//			rs=prest.executeQuery();
//			while(rs.next())
//			{
//				p.setPub_id(rs.getInt("PUB_ID"));
//				p.setTitle(rs.getString("TITLE"));
//				p.setPrice(rs.getDouble("PRICE"));
//			}
//		}
//		catch (SQLException e) 
//        { 
//            e.printStackTrace();
//		}
//		return p;
//	}
	
	public Publication viewByID(int id)
	{
            init_db();
            Publication p= new Publication();     
            String str = "Select * from publication where PUB_ID like ?";
            try
            { 
                prest =con.prepareStatement(str);
                prest.setInt(1,id);
                rs=prest.executeQuery();
                while(rs.next())
                {
                        p.setPub_id(rs.getInt("PUB_ID"));
                        p.setTitle(rs.getString("TITLE"));
                        p.setPrice(rs.getDouble("PRICE"));
                }
//                DBConnection.con.close();
                //System.out.println("PubID: " + p );
            }
	    catch (SQLException e) 
	        { 
	            e.printStackTrace();
			}
		return p;
	}
	
	
	
	public List<Publication> viewAll()
	{
		init_db();
		List <Publication> pub= new ArrayList<Publication>();
		String str = "Select * from publication";
		try
		{ 
			stmt= con.createStatement();
			rs= stmt.executeQuery(str);
			while (rs.next())
			{
				Publication p= new Publication();
				p.setPub_id(rs.getInt("PUB_ID"));
				p.setTitle(rs.getString("TITLE"));
				p.setPrice(rs.getDouble("PRICE"));
				pub.add(p);
			}
		}
	    catch (SQLException e) 
        { 
            JOptionPane.showMessageDialog(null,"Error: failed to display all products."
            		+ "\n" + e.getMessage() + "\n" + str); 
            e.printStackTrace();
		}
		return pub;
	}
	
	public static void init_db()
	{
        try
        {
        	//jdbc:mysql://localhost:3306
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url="jdbc:mysql://localhost:3307/newsagent";
            con = DriverManager.getConnection(url, "root", "admin");
            stmt = con.createStatement(); 
            System.out.println("Database Connection Successful");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Error: Failed to connect to database\n"+e.getMessage());
        }          
	}
	
}
