
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mariel
 */


public class OrderDAO {
    
    static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	private static PreparedStatement prest=null;
    public List<Order> viewAll()
	{
		init_db();
		List <Order> pub= new ArrayList<Order>();
		String str = "Select * from orders";
		try
		{ 
			stmt= con.createStatement();
			rs= stmt.executeQuery(str);
			while (rs.next())
			{
				Order p= new Order();
				p.setCus_id(rs.getInt("CUST_ID"));
				p.setPub_id(rs.getInt("PUB_ID"));
				pub.add(p);
			}
		}
	    catch (SQLException e) 
        { 
            JOptionPane.showMessageDialog(null,"Error: failed to display all products."
            		+ "\n" + e.getMessage() + "\n" + str); 
            e.printStackTrace();
		}
		return pub;
	}
    
    	public static void init_db()
	{
        try
        {
        	//jdbc:mysql://localhost:3306
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url="jdbc:mysql://localhost:3307/newsagent";
            con = DriverManager.getConnection(url, "root", "admin");
            stmt = con.createStatement(); 
            System.out.println("Database Connection Successful");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Error: Failed to connect to database\n"+e.getMessage());
        }          
	}
	
}
