/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mariel
 */
public class Order {
    
    private int cus_id;
    private int pub_id;

    public Order(int cus_id, int pub_id) {
        this.cus_id = cus_id;
        this.pub_id = pub_id;
    }

    Order() {
     
    }

    public int getCus_id() {
        return cus_id;
    }

    public void setCus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public int getPub_id() {
        return pub_id;
    }

    public void setPub_id(int pub_id) {
        this.pub_id = pub_id;
    }
    
}
