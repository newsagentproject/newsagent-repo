
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mariel
 */
public class StaffDAO {
    static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	private static PreparedStatement prest=null;
        private static PreparedStatement prest2=null;
        private static PreparedStatement prest3=null;
	
       // DBConnection db= new DBConnection();
        
//	public PublicationDAO()
//	{
//		con=DBConnection.getConnection();
//	}

        public Staff updateStaff(Staff s)//done
	{	
            init_db();
            //Publication p= new Publication();
		try 
		{
	        String sql = "UPDATE Staff SET FIRSTNAME = ?, LASTNAME=?, Eir_code=?, ADDRESS1=?, ADDRESS2=?, Town=?, County=? WHERE STAFF_ID = ?" ;
                prest = con.prepareStatement(sql);	
			//prest.setInt(1,pub.getPub_id());
			prest.setString(1,s.getFirstName());
			prest.setString(2, s.getLastName());
                        prest.setString(3, s.getEir_Code());
                        prest.setString(4, s.getAddress1());
                        prest.setString(5, s.getAddress2());
                        prest.setString(6, s.getTown());
                        prest.setString(7, s.getCounty());
                        prest.setInt(8,s.getStaff_id());
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Update Success!");
		}
		catch (SQLException err) 
                {
			System.out.println(err.getMessage());
		}
                return s;
	}
	public void addStaff(Staff s)
	{	
		init_db();
		try 
		{
	        String sql = "INSERT INTO staff ( STAFF_ID, FIRSTNAME, LASTNAME, Eir_code, ADDRESS1, ADDRESS2, Town, County) VALUES (null,?,?,?,?,?,?,?)" ;
                prest = con.prepareStatement(sql);	
			//prest.setInt(1,pub.getPub_id());
			prest.setString(1, s.getFirstName());
			prest.setString(2, s.getLastName());
                        prest.setString(3, s.getEir_Code());
                        prest.setString(4, s.getAddress1());
                        prest.setString(5, s.getAddress2());
                        prest.setString(6, s.getTown());
                        prest.setString(7, s.getCounty());
                        //prest.setInt(8,s.getStaff_id());
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Insert Success!");
		}
		catch (SQLException err) {
			System.out.println(err.getMessage());
		}
	}
	
	public static void deleteStaff(int id)
	{	
		init_db();
		
		try 
		{
	        String sql = "DELETE from staff WHERE STAFF_ID=" + id ;
                String sql2 = "DELETE from delivery WHERE STAFF_ID=" + id ;
                prest2 = con.prepareStatement(sql2);
                prest = con.prepareStatement(sql);	
			//prest.setInt(1,id);
                        prest2.executeUpdate();
			prest.executeUpdate();
			JOptionPane.showMessageDialog(null,"Deletion Success!");
		}
		catch (SQLException err) {
			System.out.println(err.getMessage());
		}
	}
	
//	public Publication viewByName(String title)
//	{
//		init_db();
//		Publication p= new Publication();
//		String str = "Select * from Publication where TITLE like ? ";
//		try
//		{ 
//			prest = con.prepareStatement(str);
//			prest.setString(1,title);
//			rs=prest.executeQuery();
//			while(rs.next())
//			{
//				p.setPub_id(rs.getInt("PUB_ID"));
//				p.setTitle(rs.getString("TITLE"));
//				p.setPrice(rs.getDouble("PRICE"));
//			}
//		}
//		catch (SQLException e) 
//        { 
//            e.printStackTrace();
//		}
//		return p;
//	}
	
	public Staff viewByID(int id)
	{
            init_db();
            Staff s= new Staff();     
            String str = "Select * from staff where STAFF_ID like ?";
            try
            { 
                prest =con.prepareStatement(str);
                prest.setInt(1,id);
                rs=prest.executeQuery();
                while(rs.next())
                {
                        s.setStaff_id(rs.getInt("STAFF_ID"));
                        s.setFirstName(rs.getString("Firstname"));
                        s.setLastName(rs.getString("Lastname"));
                        s.setEir_Code(rs.getString("Eir_code"));
                        s.setAddress1(rs.getString("ADDRESS1"));
                        s.setAddress2(rs.getString("ADDRESS2"));
                        s.setTown(rs.getString("Town"));
                        s.setLastName(rs.getString("County"));
                }
            }
	    catch (SQLException e) 
	        { 
	            e.printStackTrace();
			}
		return s;
	}
	
	
	
	public List<Staff> viewAll()
	{
		init_db();
		List <Staff> staff= new ArrayList<Staff>();
		String str = "Select * from staff";
		try
		{ 
			stmt= con.createStatement();
			rs= stmt.executeQuery(str);
			while (rs.next())
			{
                            Staff s= new Staff();
                            s.setStaff_id(rs.getInt("STAFF_ID"));
                            s.setFirstName(rs.getString("Firstname"));
                            s.setLastName(rs.getString("Lastname"));
                            s.setEir_Code(rs.getString("Eir_code"));
                            s.setAddress1(rs.getString("ADDRESS1"));
                            s.setAddress2(rs.getString("ADDRESS2"));
                            s.setTown(rs.getString("Eir_code"));
                            s.setLastName(rs.getString("Eir_code"));
                            staff.add(s);
			}
		}
	    catch (SQLException e) 
        { 
            JOptionPane.showMessageDialog(null,"Error: failed to display all products."
            		+ "\n" + e.getMessage() + "\n" + str); 
            e.printStackTrace();
		}
		return staff;
	}
	
	public static void init_db()
	{
        try
        {
        	//jdbc:mysql://localhost:3306
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url="jdbc:mysql://localhost:3307/newsagent";
            con = DriverManager.getConnection(url, "root", "admin");
            stmt = con.createStatement(); 
            System.out.println("Database Connection Successful");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Error: Failed to connect to database\n"+e.getMessage());
        }          
	}
    
}
