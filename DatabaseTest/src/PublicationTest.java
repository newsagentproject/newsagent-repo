import java.sql.SQLException;
import junit.framework.TestCase;

public class PublicationTest extends TestCase 
{
	// Test #: 001
	// Objective: Display publication (Daily Mail)
	// Input: 1 (valid)
	// Expected output: Daily Mail
	
	public void test001() throws SQLException
	{
		NewsagentDAO n1 = new NewsagentDAO();
		try 
		{
			assertEquals(n1.getPublication(1), "Daily Mail");
		} 
		catch (NewsagentExceptionHandler e)
		{
			assertEquals("Cannot enter a value less than one or a value greater than 5", e.getMessage());
		}
	}
	
	// Test #: 002
	// Objective: Invalid input at first boundary
	// Input: 0 (invalid)
	// Expected output: Exception
	public void test002() throws SQLException
	{
		NewsagentDAO na = new NewsagentDAO();
		try 
		{
			na.getPublication(0);
		} 
		catch (NewsagentExceptionHandler e)
		{
			assertEquals("Cannot enter a value less than one or a value greater than 5", e.getMessage());
		}
	}
	
	// Test #: 003
	// Objective: Display publication (Irish Examiner)
	// Input: 2 (valid)
	// Expected output: Irish Examiner
		
	public void test003() throws SQLException
	{
		NewsagentDAO n1 = new NewsagentDAO();
		try 
		{
			assertEquals(n1.getPublication(2), "Irish Examiner");
		} 
		catch (NewsagentExceptionHandler e)
		{
			assertEquals("Cannot enter a value less than one or a value greater than 5", e.getMessage());
		}
	}
	
	// Test #: 004
	// Objective: Display publication at final boundary (Irish Independent (Current last publication)
	// Input: totalPublications() to get the amount of publications (valid)
	// Expected output: Irish Examiner
		
	public void test004() throws SQLException
	{
		NewsagentDAO n1 = new NewsagentDAO();
		try 
		{
			assertEquals(n1.getPublication(n1.totalPublications()), "Irish Independent");
		} 
		catch (NewsagentExceptionHandler e)
		{
			assertEquals("Cannot enter a value less than one or a value greater than 5", e.getMessage());
		}
	}
	
	// Test #: 005
	// Objective: Display publication at final boundary - 1 (Westmeath Express (Current last publication - 1)
	// Input: totalPublications() - 1, to get the amount of publications - 1 (valid)
	// Expected output: Westmeath Express
		
	public void test005() throws SQLException
	{
		NewsagentDAO n1 = new NewsagentDAO();
		try 
		{
			assertEquals(n1.getPublication(n1.totalPublications() - 1), "Westmeath Express");
		} 
		catch (NewsagentExceptionHandler e)
		{
			assertEquals("Cannot enter a value less than one or a value greater than 5", e.getMessage());
		}
	}
	
	// Test #: 006
	// Objective: Invalid entry at final boundary + 1 (Invalid)
	// Input: totalPublications() + 1, to get the amount of publications + 1 (invalid)
	// Expected output: Exception
		
	public void test006() throws SQLException
	{
		NewsagentDAO n1 = new NewsagentDAO();
		try 
		{
			n1.getPublication(n1.totalPublications() + 1);
		} 
		catch (NewsagentExceptionHandler e)
		{
			assertEquals("Cannot enter a value less than one or a value greater than 5", e.getMessage());
		}
	}
}
