import java.sql.*;
public class NewsagentDAO
{
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;

	public NewsagentDAO()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://localhost:3306/newsagent?useSSL=false";
			con = DriverManager.getConnection(url, "adam", "admin");
			stmt = con.createStatement();
		}
		catch (Exception e)
		{
			System.out.println("Failed to connect to Database " + e.getMessage());
		}
	}
	
	public static String getPublication(int idNumber) throws NewsagentExceptionHandler, SQLException
	{
		if(idNumber < 1 || idNumber > 5)
		{
			throw new NewsagentExceptionHandler("Cannot enter a value less than one or a value greater than 5");
		}
		else
		{
			rs = stmt.executeQuery("Select title from publication where pub_id = " + idNumber);
			rs.next();
			String publication = rs.getString("title");
			System.out.println(publication);
			return publication;
		}
	}
	public static int totalPublications() throws SQLException
	{
		int noOfPublications;
		rs = stmt.executeQuery("Select count(*) as total from publication");
		rs.next();
		noOfPublications = rs.getInt("total");
		System.out.println("Number of Publications: " + noOfPublications);
		return noOfPublications;
	}
	public static void getCustomers() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from customer");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total customers: " + myTotal);
	}
	
	/*public static void main(String[] args) throws NewsagentExceptionHandler, SQLException 
	{
		try
		{
			NewsagentDAO n1 = new NewsagentDAO();
			getCustomers();
			getPublication(1);
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to get records");
		}
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}*/
}
