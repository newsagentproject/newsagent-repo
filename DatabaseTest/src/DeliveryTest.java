import junit.framework.TestCase;


public class DeliveryTest extends TestCase {

	//Test# 001
	//Test objective: valid delivery_id
	//Input(s): 1
	//Expected output(s) true
	
	public void testaddDelivery001()
	{
		//public boolean addDelivery(int deli_id,double price,String date,int bill_id,int staff_id)
		
		Delivery d = new Delivery();
		assertEquals(true,d.addDelivery(1,12.0,20,2,2016,12,23));
	}
	
	//Test# 002
	//Test objective: invalid delivery_id
	//Input(s): -1
	//Expected output(s) false
	
	public void testaddDelivery002()
	{
		//public boolean addDelivery(int deli_id,double price,String date,int bill_id,int staff_id)
		
		Delivery d = new Delivery();
		assertEquals(false,d.addDelivery(-1,12.0,20,2,2007,12,23));
	}
	
	//Test# 003
	//Test objective: valid price
	//Input(s): 0.01
	//Expected output(s) true
	
	public void testaddDelivery003()
	{
		//public boolean addDelivery(int deli_id,double price,String date,int bill_id,int staff_id)
		
		Delivery d = new Delivery();
		assertEquals(true,d.addDelivery(1,0.01,20,2,2016,12,23));
	}
	
	//Test# 004
	//Test objective: invalid price
	//Input(s): -0.99
	//Expected output(s) false
	
	public void testaddDelivery004()
	{
		//public boolean addDelivery(int deli_id,double price,String date,int bill_id,int staff_id)
		
		Delivery d = new Delivery();
		assertEquals(false,d.addDelivery(1,-0.01,20,2,2016,12,23));
	}
	
	//Test# 005
	//Test objective: valid bill_id
	//Input(s): 10
	//Expected output(s) true
	
	public void testaddDelivery005()
	{
		//public boolean addDelivery(int deli_id,double price,String date,int bill_id,int staff_id)
		
		Delivery d = new Delivery();
		assertEquals(true,d.addDelivery(1,12.0,20,2,2016,10,23));
	}
	
	//Test# 006
	//Test objective: invalid bill_id
	//Input(s): -1
	//Expected output(s) false
	
	public void testaddDelivery006()
	{
		//public boolean addDelivery(int deli_id,double price,String date,int bill_id,int staff_id)
		
		Delivery d = new Delivery();
		assertEquals(false,d.addDelivery(1,12.0,20,2,2016,-1,23));
	}
	
	//Test# 007
	//Test objective: valid staff_id
	//Input(s): 1000
	//Expected output(s) true
	
	public void testaddDelivery007()
	{
		//public boolean addDelivery(int deli_id,double price,String date,int bill_id,int staff_id)
		
		Delivery d = new Delivery();
		assertEquals(true,d.addDelivery(1,12.0,20,2,2016,10,1000));
	}
	
	//Test# 008
	//Test objective: invalid staff_id
	//Input(s): -1
	//Expected output(s) false
	
	public void testaddDelivery008()
	{
		//public boolean addDelivery(int deli_id,double price,String date,int bill_id,int staff_id)
		
		Delivery d = new Delivery();
		assertEquals(false,d.addDelivery(1,12.0,20,2,2016,10,-1));
	}
}
