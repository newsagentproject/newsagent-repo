import java.util.Scanner;
import java.util.Date;
import java.sql.*;
public class Modelsexample
{
	public static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	Scanner in = new Scanner(System.in);
	
	public Modelsexample()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://localhost:3306/newsagent?useSSL=false";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
			System.out.println("Connection Successful");
		}
		catch (Exception e)
		{
			System.out.println("Failed to connect to Database " + e.getMessage());
		}
	}
	
	public static String getPublication(int idNumber) throws NewsagentExceptionHandler, SQLException
	{
		if(idNumber < 1 || idNumber > 5)
		{
			throw new NewsagentExceptionHandler("Cannot enter a value less than one or a value greater than 5");
		}
		else
		{
			rs = stmt.executeQuery("Select title from publication where pub_id = " + idNumber);
			rs.next();
			String publication = rs.getString("title");
			System.out.println(publication);
			return publication;
		}
	}
	public static int totalPublications() throws SQLException
	{
		int noOfPublications;
		rs = stmt.executeQuery("Select count(*) as total from publication");
		rs.next();
		noOfPublications = rs.getInt("total");
		System.out.println("Number of Publications: " + noOfPublications);
		return noOfPublications;
	}
	public static void getCustomers() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from customer");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total customers: " + myTotal);
	}
	
	public static void getDeliveries() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from delivery");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total deliveries: " + myTotal);
	}
	
	public static void getOrders() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from orders");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total orders: " + myTotal);
	}
	
	public static void getStaff() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from staff");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total staff: " + myTotal);
	}
	
	public static void getBill() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from bill");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total bills: " + myTotal);
	}
	
	public boolean showPublications() throws SQLException
	{
		rs = stmt.executeQuery("Select * from publication");
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		for(int x = 1; x < columnsNumber-1; x++)
		{
			System.out.print(rsmd.getColumnName(x) + ", ");
		}
		System.out.println("");
		while (rs.next()) 
		{
			for (int i = 1; i < columnsNumber; i++) 
			{
				if (i > 1) System.out.print(",  ");
		        String columnValue = rs.getString(i);
		        System.out.print(columnValue + "");
		    }
		    System.out.println("");
		}
		return true;
	}
	
	public boolean showCustomers() throws SQLException
	{
		rs = stmt.executeQuery("Select * from customer");
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		for(int x = 1; x < columnsNumber-1; x++)
		{
			System.out.print(rsmd.getColumnName(x) + ", ");
		}
		System.out.println("");
		while (rs.next()) 
		{
			for (int i = 1; i < columnsNumber; i++) 
			{
				if (i > 1) System.out.print(",  ");
		        String columnValue = rs.getString(i);
		        System.out.print(columnValue + "");
		    }
		    System.out.println("");
		}
		return true;
	}
	
	public boolean showDeliveries() throws SQLException
	{
		rs = stmt.executeQuery("Select * from delivery");
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		for(int x = 1; x < columnsNumber-1; x++)
		{
			System.out.print(rsmd.getColumnName(x) + ", ");
		}
		System.out.println("");
		while (rs.next()) 
		{
			for (int i = 1; i < columnsNumber; i++) 
			{
				if (i > 1) System.out.print(",  ");
		        String columnValue = rs.getString(i);
		        System.out.print(columnValue + "");
		    }
		    System.out.println("");
		}
		return true;
	}
	
	public boolean showStaff() throws SQLException
	{
		rs = stmt.executeQuery("Select * from staff");
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		for(int x = 1; x < columnsNumber-1; x++)
		{
			System.out.print(rsmd.getColumnName(x) + ", ");
		}
		System.out.println("");
		while (rs.next()) 
		{
			for (int i = 1; i < columnsNumber; i++) 
			{
				if (i > 1) System.out.print(",  ");
		        String columnValue = rs.getString(i);
		        System.out.print(columnValue + "");
		    }
		    System.out.println("");
		}
		return true;
	}
	
	public void addPublication() throws SQLException, NewsagentExceptionHandler
	{
		System.out.println("Please enter a title for the publication: ");
		String title = in.next();
		System.out.println("Please enter a price for the publication: ");
		double price = in.nextDouble();
		String insert = "INSERT INTO publication VALUES(0, '"+title+"' , "+price+");"; 
		stmt.executeUpdate(insert);
		System.out.println("Successfully added!");
	}
	public void deletePublication() throws SQLException, NewsagentExceptionHandler
	{	
		int id;
		String delete;
		System.out.println("Enter the id number to delete the publication");
		if(in.hasNextInt())
		{
			id = in.nextInt();
			delete = "Delete from publication where pub_id = " +id;
			stmt.executeUpdate(delete);
			System.out.println("Successfully deleted!");
		}
		else
		{
			System.out.println("Invalid");
		} 
	}
	public void addCustomer() throws SQLException, NewsagentExceptionHandler
	{
		System.out.println("Please enter the first name of the customer: ");
		String fName = in.next();
		System.out.println("Please enter the last name of the customer: ");
		String lName = in.next();
		System.out.println("Please enter their eircode: ");
		int eCode = in.nextInt();
		System.out.println("Now enter address1: ");
		String address1 = in.next();
		System.out.println("Now enter address2: ");
		String address2 = in.next();
		System.out.println("Now enter town: ");
		String town = in.next();
		System.out.println("Now enter county: ");
		String county = in.next();
		String insert = "INSERT INTO customer VALUES(0, '"+fName+"' , '"+lName+"', '"+eCode+"', '"+address1+"', '"+address2+"', '"+town+"', '"+county+"');"; 
		stmt.executeUpdate(insert);
		System.out.println("Successfully added!");
	}
	public void deleteCustomer() throws SQLException, NewsagentExceptionHandler
	{	
		int id;
		String delete;
		System.out.println("Enter the id number to delete the customer");
		if(in.hasNextInt())
		{
			id = in.nextInt();
			delete = "Delete from customer where cus_id = " +id;
			stmt.executeUpdate(delete);
			System.out.println("Successfully deleted!");
		}
		else
		{
			System.out.println("Invalid");
		} 
	}
	public void addStaff() throws SQLException, NewsagentExceptionHandler
	{
		System.out.println("Please enter the first name of the employee: ");
		String fName = in.next();
		System.out.println("Please enter the last name of the employee: ");
		String lName = in.next();
		System.out.println("Please enter their eircode: ");
		String eCode = in.next();
		System.out.println("Now enter address1: ");
		String address1 = in.next();
		System.out.println("Now enter address2: ");
		String address2 = in.next();
		System.out.println("Now enter town: ");
		String town = in.next();
		System.out.println("Now enter county: ");
		String county = in.next();
		String insert = "INSERT INTO staff VALUES(0, '"+fName+"' , '"+lName+"', '"+eCode+"', '"+address1+"', '"+address2+"', '"+town+"', '"+county+"');"; 
		stmt.executeUpdate(insert);
		System.out.println("Successfully added!");
	}
	public void deleteStaff() throws SQLException, NewsagentExceptionHandler
	{	
		int id;
		String delete;
		System.out.println("Enter the id number to delete the employee");
		if(in.hasNextInt())
		{
			id = in.nextInt();
			delete = "Delete from staff where staff_id = " +id;
			stmt.executeUpdate(delete);
			System.out.println("Successfully deleted!");
		}
		else
		{
			System.out.println("Invalid");
		} 
	}
	public void addDelivery() throws SQLException, NewsagentExceptionHandler
	{
		Date date = new Date();
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		//System.out.printf("%2$tB %2$td, %2$tY", "Due date:", date);
		//String dueDate = "CURDATE()";
		System.out.println(sqlDate);
		System.out.println("Please enter the price of delivery: ");
		double price = in.nextDouble();
		String insert = "INSERT INTO delivery VALUES(0, '"+sqlDate+"' , '"+price+"', 1, 1000);"; 
		stmt.executeUpdate(insert);
		System.out.println("Successfully added!");
	}
	public void deleteDelivery() throws SQLException, NewsagentExceptionHandler
	{	
		int id;
		String delete;
		System.out.println("Enter the id number to delete the delivery");
		if(in.hasNextInt())
		{
			id = in.nextInt();
			delete = "Delete from delivery where delivery_id = " +id;
			stmt.executeUpdate(delete);
			System.out.println("Successfully deleted!");
		}
		else
		{
			System.out.println("Invalid");
		} 
	}
	public static void main(String[] args) throws NewsagentExceptionHandler, SQLException 
	{
		try
		{
			getCustomers();
			getPublication(1);
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to get records");
		}
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
}