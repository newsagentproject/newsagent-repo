import java.sql.*;
public class CustomerModel
{
	public static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;

	public CustomerModel()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://localhost:3307/newsagent?useSSL=false";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
			System.out.println("Connection Successful");
		}
		catch (Exception e)
		{
			System.out.println("Failed to connect to Database " + e.getMessage());
		}
	}
	public static void getCustomers() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from customer");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total customers: " + myTotal);
	}
	
	
	public boolean showCustomers() throws SQLException
	{
		rs = stmt.executeQuery("Select * from customer");
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		for(int x = 1; x < columnsNumber-1; x++)
		{
			System.out.print(rsmd.getColumnName(x) + ", ");
		}
		System.out.println("");
		while (rs.next()) 
		{
			for (int i = 1; i < columnsNumber; i++) 
			{
				if (i > 1) System.out.print(",  ");
		        String columnValue = rs.getString(i);
		        System.out.print(columnValue + "");
		    }
		    System.out.println("");
		}
		return true;
	}
	

	
	public static void main(String[] args) throws NewsagentExceptionHandler, SQLException 
	{
		try
		{
			getCustomers();
		
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to get records");
		}
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
}