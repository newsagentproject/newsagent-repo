package Publication;
public class Publications {

	private int pub_id;
	private String title;
	private double  price;
	
	public Publications()
	{
		this.pub_id = 1;
		this.title = "Sunday Times";
		this.price = 1.29;
	}

	public int getPub_id() {
		return pub_id;
	}

	public void setPub_id(int pub_id) {
		this.pub_id = pub_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public String toString()
	{
		return "Pub ID: " +pub_id+ "Title: " +title+ "Price: " +price+ "";
	}
}
