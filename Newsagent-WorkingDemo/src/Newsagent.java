import java.sql.SQLException;
import java.util.Scanner;

import Connection.NewsagentDAO;
import Connection.NewsagentExceptionHandler;


public class Newsagent {

	public static void main(String[] args) throws SQLException, NewsagentExceptionHandler {
		// TODO Auto-generated method stub
		NewsagentDAO dao = new NewsagentDAO();
		//dao.getCustomers();
		//dao.getPublication(1);
		//dao.getPublication(2);
		//dao.totalPublications();
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("1. View All Customers");
		System.out.println("2. View All Publications");
		System.out.println("3. View All Deliveries");
		System.out.println("4. View All Orders");
		System.out.println("5. View All Staff");
		System.out.println("6. View All Bills");
		
		int choice = in.nextInt();
		
			if(choice == 1)
			{
				dao.getCustomers();
			}
			if(choice == 2)
			{
				for(int i = 0; i < 4; i++)
				{
					dao.getPublication(i);
				}
			}
			if(choice == 3)
			{
				dao.getDeliveries();
			}
			if(choice == 4)
			{
				dao.getOrders();
			}
			if(choice == 5)
			{
				dao.getStaff();
			}
			if(choice == 6)
			{
				dao.getBill();
			}
	}
}
