package Delivery;
import Connection.NewsagentDAO;
import java.sql.PreparedStatement;

import Connection.NewsagentDAO;


public class DeliveryJDBC extends NewsagentDAO {


	public static void addDelivery(Delivery D)
	{
		try{
			String str = "Insert into Delivery Values(null,1.0,1,1,2016,100,100)";
			PreparedStatement psmt = con.prepareStatement(str);
			
			psmt.setInt(1,D.getDelivery_id());
			psmt.setDouble(1,D.getPrice());
			psmt.setString(1,D.getDate());
			psmt.setInt(1,D.getBill_id());
			psmt.setInt(1, D.getStaff_id());
			
			int num = psmt.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println("Error adding Delivery");
			e.printStackTrace();
		}
	}
	
	public static void displayDelivery(Delivery D1)
	{
		try{
		System.out.println("Delivery ID: " + D1.getDelivery_id());
		System.out.println("Price: " + D1.getPrice());
		System.out.println("Date: " +D1.getDate());
		System.out.println("Bill ID: " +D1.getBill_id());
		System.out.println("Staff ID: " +D1.getStaff_id());
		}
		catch(Exception e)
		{
			System.out.println("Error displaying delivery");
			e.printStackTrace();
		}
	}
	
	public static void main(String [] args)
	{
		Delivery d = new Delivery();
		displayDelivery(d);
	}
}