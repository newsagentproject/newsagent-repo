package Delivery;
import java.util.Date;
public class Delivery
{
	private static int delivery_id;
	private double price;
	private int bill_id;
	private int staff_id;
	private int day;
	private int month;
	private int year;
	private int lastAssigned = 0;
	
	
	public static boolean addDelivery(int deli_id,double price,int day,int month, int year,int bill_id,int staff_id)
	{
		boolean test = true;
		
		if(deli_id < 1)
		{
			test = false;
		}
		
		if(price < 0)
		{
			test = false;
		}
		
		if(day < 0 || day > 31)
		{
			test = false;
		}
		
		if(month < 0 || month > 12)
		{
			test = false;
		}
		
		if(year < 2016 || year > 2018)
		{
			test = false;
		}
		
		if(bill_id < 0)
		{
			test = false;
		}
		if(staff_id < 0)
		{
			test = false;
		}
		return test;
	}
	public Delivery()
	{
		this.delivery_id = 10;
		this.price = 20.00;
		this.bill_id = 20;
		
		lastAssigned++;
		this.staff_id = lastAssigned;
	}
	
	public void print()
	{
		System.out.println("Delivery ID: " + delivery_id);
		System.out.println("Price: " + price);
		System.out.println("Date: " + getDate());
		System.out.println("Bill ID: " + bill_id);
		System.out.println("Staff ID: " + staff_id);
	}
	
	//Getters
	
	
	public double getPrice() {
		return price;
	}

	public String getDate() {
		return "" +day+ "" +month+ "" +year+ "";
	}

	public int getBill_id() {
		return bill_id;
	}

	public int getStaff_id() {
		return staff_id;
	}

	public int getLastAssigned() {
		return lastAssigned;
	}
	
	public int getDelivery_id() {
		return delivery_id;
	}
	
	//Setters
	
	public void setPrice(double price) {
		this.price = price;
	}

	public void setBill_id(int bill_id) {
		this.bill_id = bill_id;
	}

	public void setStaff_id(int staff_id) {
		this.staff_id = staff_id;
	}

	public void setLastAssigned(int lastAssigned) {
		this.lastAssigned = lastAssigned;
	}

}