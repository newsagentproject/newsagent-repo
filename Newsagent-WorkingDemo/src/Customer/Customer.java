package Customer;
public class Customer {

	private int cus_id;
	private String firstName;
	private String lastName;
	private String eir_code;
	private String address1;
	private String address2;
	private String town;
	private String county;
	
	public Customer()
	{
		this.cus_id = 1;
		this.firstName = "John";
		this.lastName = "Doe";
		this.eir_code = "R35 RF77";
		this.address1 = "Screggan";
		this.address2 = "";
		this.town = "Tullamore";
		this.county = "Offaly";
	}
	
	public void setCus_id(int id)
	{
		this.cus_id = id;
	}
	
	public int getCus_id()
	{
		return cus_id;
	}
	
	public void setFirstName(String firstname)
	{
		this.firstName = firstname;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	
	public void setLastName(String lastname)
	{
		this.lastName = lastname;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public void setEir_Code(String code)
	{
		this.eir_code = code;
	}
	
	public String getEir_Code()
	{
		return eir_code;
	}
	
	public void setAddress1(String addr1)
	{
		this.address1 = addr1;
	}
	
	public String getAddress1()
	{
		return address1;
	}
	
	public void setAddress2(String addr2)
	{
		this.address2 = addr2;
	}
	
	public String getAddress2()
	{
		return address2;
	}
	
	public void setTown(String town)
	{
		this.town = town;
	}
	
	public String getTown()
	{
		return town;
	}
	
	public void setCounty(String county){
		this.county = county;
	}
	
	public String getCounty()
	{
		return county;
	}
	
	public String toString()
	{
		return "Cus Id: " +cus_id+ "First Name: " +firstName+ "\n Last Name: " +lastName+ "Eir Code: " +eir_code+ "Address 1: " +address1+ "Adress 2: " +address2+ "Town: " +town+ "County: " +county+ "";
	}
	
	public void printDetails()
	{
		System.out.println("Cus id: " +cus_id);
		System.out.println("First Name: " +firstName);
		System.out.println("Last Name: " +lastName);
		System.out.println("Eir Code: " +eir_code);
		System.out.println("Address 1: " +address1);
		System.out.println("Address 2: " +address2);
		System.out.println("Town: " +town);
		System.out.println("County: " +county);
	}
}