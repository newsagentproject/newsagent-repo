import java.sql.SQLException;
import java.util.Scanner;


public class View {

	public static void main(String[] args) throws SQLException, NewsagentExceptionHandler
	{
	

		CustomerModel c=new CustomerModel();
		StaffModel s=new StaffModel();
		PublicationModel p=new PublicationModel();
		DeliveryModel d=new DeliveryModel();
		Scanner in = new Scanner(System.in);
		
		System.out.println("1. Display Customers");
		System.out.println("2. Total Customer");
		System.out.println("3. Add Customer");
		System.out.println("4. delete Customer");
		System.out.println("5. Display Staff");
		System.out.println("6. Total Staff");
		System.out.println("7. Add Staff");
		System.out.println("8. delete Staff");
		System.out.println("9. Display Publication");
		System.out.println("10. Total Publication");
		System.out.println("11. Add Publication");
		System.out.println("12. delete Publication");
		System.out.println("13. Display Delivery");
		System.out.println("14. Total Delivery");
		System.out.println("15. Add Delivery");
		System.out.println("16. delete Delivery");
		
		int choice = in.nextInt();
		
		while(choice != -1)
		{
			if(choice == 1)
			{
				
			c.showCustomers();
			}
			if(choice == 2)
			{
				c.getCustomers();
			}
			if(choice == 3)
			{
				c.addCustomer();
			}
			if(choice == 4)
			{
				c.deleteCustomer();
			}
			if(choice == 5)
			{
				
			s.showstaff();
			}
			if(choice == 6)
			{
				s.getstaff();
			}
			if(choice == 7)
			{
			s.addStaff();
			}
			if(choice == 8)
			{
				s.deleteStaff();
			}
			if(choice == 9)
			{
				p.showpublications();
			}
			if(choice == 10)
			{
				
			p.getPublications();
			}
			if(choice == 11)
			{
				p.addPublication();
			}
			if(choice == 12)
			{
			p.deletePublication();
			}
			if(choice == 13)
			{
				d.showDeliveries();
			}
			if(choice == 14)
			{
				d.getDeliveries();
			}
			if(choice == 15)
			{
				
			d.addDelivery();
			}
			if(choice == 16)
			{
				d.deleteDelivery();
			}
			
			
			choice = in.nextInt();
		}
	}
}