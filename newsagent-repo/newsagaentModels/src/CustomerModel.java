import java.sql.*;
import java.util.Scanner;
public class CustomerModel
{
	public static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	Scanner in = new Scanner(System.in);
	public CustomerModel()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://localhost:3307/newsagent?useSSL=false";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
			System.out.println("Connection Successful");
		}
		catch (Exception e)
		{
			System.out.println("Failed to connect to Database " + e.getMessage());
		}
	}
	public static void getCustomers() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from customer");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total customers: " + myTotal);
	}
	
	
	public void showCustomers() throws SQLException
	{
		rs = stmt.executeQuery("Select * from customer");
		rs.next();
		String disfir = rs.getString("firstname");
		String dislas = rs.getString("lastname");
		String diseCode = rs.getString("Eir_code");
		String disAd1 = rs.getString("Address1");
		String disAd2 = rs.getString("Address2");
		String distown = rs.getString("Town");
		String disCounty = rs.getString("county");
		System.out.println(disfir+" "+dislas+" "+diseCode+" "+disAd1+" "+disAd2+" "+distown+" "+disCounty);
		
	}
	
	public void addCustomer() throws SQLException, NewsagentExceptionHandler
	{
		System.out.println("Please enter the first name of the customer: ");
		String fName = in.next();
		System.out.println("Please enter the last name of the customer: ");
		String lName = in.next();
		System.out.println("Please enter their eircode: ");
		int eCode = in.nextInt();
		System.out.println("Now enter address1: ");
		String address1 = in.next();
		System.out.println("Now enter address2: ");
		String address2 = in.next();
		System.out.println("Now enter town: ");
		String town = in.next();
		System.out.println("Now enter county: ");
		String county = in.next();
		String insert = "INSERT INTO customer VALUES(0, '"+fName+"' , '"+lName+"', '"+eCode+"', '"+address1+"', '"+address2+"', '"+town+"', '"+county+"');"; 
		stmt.executeUpdate(insert);
		System.out.println("Successfully added!");
	}
	public void deleteCustomer() throws SQLException, NewsagentExceptionHandler
	{	
		int id;
		String delete;
		System.out.println("Enter the id number to delete the customer");
		if(in.hasNextInt())
		{
			id = in.nextInt();
			delete = "Delete from customer where cus_id = " +id;
			stmt.executeUpdate(delete);
			System.out.println("Successfully deleted!");
		}
		else
		{
			System.out.println("Invalid");
		} 
	}

	
	public static void main(String[] args) throws NewsagentExceptionHandler, SQLException 
	{
		try
		{
			getCustomers();
		
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to get records");
		}
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
}