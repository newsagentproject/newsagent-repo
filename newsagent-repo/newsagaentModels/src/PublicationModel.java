import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
public class PublicationModel
{
	public static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	Scanner in=new Scanner(System.in);
	public PublicationModel()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://localhost:3307/newsagent?useSSL=false";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
			System.out.println("Connection Successful");
		}
		catch (Exception e)
		{
			System.out.println("Failed to connect to Database " + e.getMessage());
		}
	}
	public static void getPublications() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from publication");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total customers: " + myTotal);
	}
	
	
	public boolean showpublications() throws SQLException
	{
		rs = stmt.executeQuery("Select * from publication");
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		for(int x = 1; x < columnsNumber-1; x++)
		{
			System.out.print(rsmd.getColumnName(x) + ", ");
		}
		System.out.println("");
		while (rs.next()) 
		{
			for (int i = 1; i < columnsNumber; i++) 
			{
				if (i > 1) System.out.print(",  ");
		        String columnValue = rs.getString(i);
		        System.out.print(columnValue + "");
		    }
		    System.out.println("");
		}
		return true;
	}
	public void addPublication() throws SQLException, NewsagentExceptionHandler
	{
		System.out.println("Please enter a title for the publication: ");
		String title = in.next();
		System.out.println("Please enter a price for the publication: ");
		double price = in.nextDouble();
		String insert = "INSERT INTO publication VALUES(0, '"+title+"' , "+price+");"; 
		stmt.executeUpdate(insert);
		System.out.println("Successfully added!");
	}
	public void deletePublication() throws SQLException, NewsagentExceptionHandler
	{	
		int id;
		String delete;
		System.out.println("Enter the id number to delete the publication");
		if(in.hasNextInt())
		{
			id = in.nextInt();
			delete = "Delete from publication where pub_id = " +id;
			stmt.executeUpdate(delete);
			System.out.println("Successfully deleted!");
		}
		else
		{
			System.out.println("Invalid");
		} 
	}

	
	public static void main(String[] args) throws NewsagentExceptionHandler, SQLException 
	{
		try
		{
			getPublications();
		
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to get records");
		}
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
}