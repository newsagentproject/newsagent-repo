import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
public class StaffModel
{
	public static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	Scanner in = new Scanner(System.in);
	public StaffModel()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://localhost:3307/newsagent?useSSL=false";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
			System.out.println("Connection Successful");
		}
		catch (Exception e)
		{
			System.out.println("Failed to connect to Database " + e.getMessage());
		}
	}
	public static void getstaff() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from staff");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total number of staff: " + myTotal);
	}
	
	
	public boolean showstaff() throws SQLException
	{
		rs = stmt.executeQuery("Select * from staff");
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		for(int x = 1; x < columnsNumber-1; x++)
		{
			System.out.print(rsmd.getColumnName(x) + ", ");
		}
		System.out.println("");
		while (rs.next()) 
		{
			for (int i = 1; i < columnsNumber; i++) 
			{
				if (i > 1) System.out.print(",  ");
		        String columnValue = rs.getString(i);
		        System.out.print(columnValue + "");
		    }
		    System.out.println("");
		}
		return true;
	}
	public void addStaff() throws SQLException, NewsagentExceptionHandler
	{
		System.out.println("Please enter the first name of the employee: ");
		String fName = in.next();
		System.out.println("Please enter the last name of the employee: ");
		String lName = in.next();
		System.out.println("Please enter their eircode: ");
		String eCode = in.next();
		System.out.println("Now enter address1: ");
		String address1 = in.next();
		System.out.println("Now enter address2: ");
		String address2 = in.next();
		System.out.println("Now enter town: ");
		String town = in.next();
		System.out.println("Now enter county: ");
		String county = in.next();
		String insert = "INSERT INTO staff VALUES(0, '"+fName+"' , '"+lName+"', '"+eCode+"', '"+address1+"', '"+address2+"', '"+town+"', '"+county+"');"; 
		stmt.executeUpdate(insert);
		System.out.println("Successfully added!");
	}
	public void deleteStaff() throws SQLException, NewsagentExceptionHandler
	{	
		int id;
		String delete;
		System.out.println("Enter the id number to delete the employee");
		if(in.hasNextInt())
		{
			id = in.nextInt();
			delete = "Delete from staff where staff_id = " +id;
			stmt.executeUpdate(delete);
			System.out.println("Successfully deleted!");
		}
		else
		{
			System.out.println("Invalid");
		} 
	}

	
	public static void main(String[] args) throws NewsagentExceptionHandler, SQLException 
	{
		try
		{
			getstaff();
		
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to get records");
		}
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
}