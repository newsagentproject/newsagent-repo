import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Scanner;
public class DeliveryModel
{
	public static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	Scanner in = new Scanner(System.in);
	public DeliveryModel()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://localhost:3307/newsagent?useSSL=false";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
			System.out.println("Connection Successful");
		}
		catch (Exception e)
		{
			System.out.println("Failed to connect to Database " + e.getMessage());
		}
	}
	public static void getDeliveries() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from delivery");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total customers: " + myTotal);
	}
	
	
	public boolean showDeliveries() throws SQLException
	{
		rs = stmt.executeQuery("Select * from delivery");
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		for(int x = 1; x < columnsNumber-1; x++)
		{
			System.out.print(rsmd.getColumnName(x) + ", ");
		}
		System.out.println("");
		while (rs.next()) 
		{
			for (int i = 1; i < columnsNumber; i++) 
			{
				if (i > 1) System.out.print(",  ");
		        String columnValue = rs.getString(i);
		        System.out.print(columnValue + "");
		    }
		    System.out.println("");
		}
		return true;
	}
	public void addDelivery() throws SQLException, NewsagentExceptionHandler
	{
		Date date = new Date();
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		//System.out.printf("%2$tB %2$td, %2$tY", "Due date:", date);
		//String dueDate = "CURDATE()";
		System.out.println(sqlDate);
		System.out.println("Please enter the price of delivery: ");
		double price = in.nextDouble();
		String insert = "INSERT INTO delivery VALUES(0, '"+sqlDate+"' , '"+price+"', 1, 1000);"; 
		stmt.executeUpdate(insert);
		System.out.println("Successfully added!");
	}
	public void deleteDelivery() throws SQLException, NewsagentExceptionHandler
	{	
		int id;
		String delete;
		System.out.println("Enter the id number to delete the delivery");
		if(in.hasNextInt())
		{
			id = in.nextInt();
			delete = "Delete from delivery where delivery_id = " +id;
			stmt.executeUpdate(delete);
			System.out.println("Successfully deleted!");
		}
		else
		{
			System.out.println("Invalid");
		} 
	}

	
	public static void main(String[] args) throws NewsagentExceptionHandler, SQLException 
	{
		try
		{
			getDeliveries();
		
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to get records");
		}
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
}