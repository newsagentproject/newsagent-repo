import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;
public class CustomerModel
{
	public static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
        Scanner in = new Scanner(System.in);
	public CustomerModel()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://localhost:3307/newsagent?useSSL=false";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
			System.out.println("Connection Successful");
		}
		catch (Exception e)
		{
			System.out.println("Failed to connect to Database " + e.getMessage());
		}
	}
	public static String getCustomers() throws SQLException
	{
		rs = stmt.executeQuery("Select count(*) as total from customer");
		rs.next();
		int myTotal = rs.getInt("total");
		System.out.println("Total customers: " + myTotal);
                return myTotal + "";
	}
	
        ArrayList<String> strings = new ArrayList<String>();
	
	public ArrayList<String> showCustomers() throws SQLException
	{
            rs = stmt.executeQuery("Select count(*) as total from customer");
            rs.next();
            int myTotal = rs.getInt("total");
            
		int id_get = 1000;
		            
                String fn = "";
                String ln = "";
                String add1 = "";
                String add2 = "";
                String ec = "";
                String twn = "";
                String cty = "";
                
		for(int i = 0; i < myTotal; i++)
		{
			rs = stmt.executeQuery("Select * from customer where cus_id = " + id_get);
			rs.next();
	
			fn = rs.getString("firstname");
			ln = rs.getString("lastname");
			add1 = rs.getString("address1");
			add2 = rs.getString("address2");
			ec = rs.getString("eir_code");
			twn = rs.getString("town");
			cty = rs.getString("county");
                        
                        id_get++;
                        String res = id_get + " " + fn + " " + ln + " " + add1 + " " + add2 + " " + ec + " " + twn + " " + cty + "\n";
                        strings.add(res);
                        System.out.println(res);
		}
            return strings;
	}
	public void addCustomer() throws SQLException, NewsagentExceptionHandler
	{
		System.out.println("Please enter the first name of the customer: ");
		String fName = in.next();
		System.out.println("Please enter the last name of the customer: ");
		String lName = in.next();
		System.out.println("Please enter their eircode: ");
		int eCode = in.nextInt();
		System.out.println("Now enter address1: ");
		String address1 = in.next();
		System.out.println("Now enter address2: ");
		String address2 = in.next();
		System.out.println("Now enter town: ");
		String town = in.next();
		System.out.println("Now enter county: ");
		String county = in.next();
		String insert = "INSERT INTO customer VALUES(0, '"+fName+"' , '"+lName+"', '"+eCode+"', '"+address1+"', '"+address2+"', '"+town+"', '"+county+"');"; 
		stmt.executeUpdate(insert);
		System.out.println("Successfully added!");
	}
	public void deleteCustomer() throws SQLException, NewsagentExceptionHandler
	{	
		int id;
		String delete;
		System.out.println("Enter the id number to delete the customer");
		if(in.hasNextInt())
		{
			id = in.nextInt();
			delete = "Delete from customer where cus_id = " +id;
			stmt.executeUpdate(delete);
			System.out.println("Successfully deleted!");
                        strings.remove(id - 1000);
		}
		else
		{
			System.out.println("Invalid");
		} 
	}

	
	public static void main(String[] args) throws NewsagentExceptionHandler, SQLException 
	{
		try
		{
			getCustomers();
		
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to get records");
		}
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
}