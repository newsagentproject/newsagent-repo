package lab;

public class staff {

	private int staff_id;
	private String firstName;
	private String lastName;
	private int eir_code;
	private String address1;
	private String address2;
	private String town;
	private String county;
	
	public staff(String firstName, int staff_id, String lastName, int eir_code, String address1, String address2, String town, String county)
	{
		this.staff_id = staff_id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.eir_code = eir_code;
		this.address1 = address1;
		this.address2 = address2;
		this.town = town;
		this.county = county;
	}
	public boolean validateCustomer(int staff_id,String firstname,String lastname,String eirCode,String addr1,String addr2,String town,String county)
	{
		boolean test=true;
		if(staff_id<1000)
		{
			test=false;
		}
		if(firstname.length()>20)
		{
			test=false;
		}
		else if(firstname.length()<2)
		{
			test=false;
		}
		if(lastname.length()>20)
		{
			test=false;
		}
		else if(lastname.length()<2)
		{
			test=false;
		}
		if(eirCode.length()!=8)
		{
			test=false;
		}
		if((addr1.length()>20)||(addr2.length()>20)||(town.length()>20)||(addr2.length()>20)||(county.length()>20))
		{
			test=false;
		}
		return test;
	}
	public void setStaff_id(int id)
	{
		this.staff_id = id;
	}
	
	public int getStaff_id()
	{
		return staff_id;
	}
	
	public void setFirstName(String firstname)
	{
		this.firstName = firstname;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	
	public void setLastName(String lastname)
	{
		this.lastName = lastname;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public void setEir_Code(int code)
	{
		this.eir_code = code;
	}
	
	public int getEir_Code()
	{
		return eir_code;
	}
	
	public void setAddress1(String addr1)
	{
		this.address1 = addr1;
	}
	
	public String getAddress1()
	{
		return address1;
	}
	
	public void setAddress2(String addr2)
	{
		this.address2 = addr2;
	}
	
	public String getAddress2()
	{
		return address2;
	}
	
	public void setTown(String town)
	{
		this.town = town;
	}
	
	public String getTown()
	{
		return town;
	}
	
	public void setCounty(String county){
		this.county = county;
	}
	
	public String getCounty()
	{
		return county;
	}
	
	public String toString()
	{
		return "Staff Id: " +staff_id+ "First Name: " +firstName+ "\n Last Name: " +lastName+ "Eir Code: " +eir_code+ "Address 1: " +address1+ "Adress 2: " +address2+ "Town: " +town+ "County: " +county+ "";
	}
	
	public void printDetails()
	{
		System.out.println("Staff ID: " +staff_id);
		System.out.println("First Name: " +firstName);
		System.out.println("Last Name: " +lastName);
		System.out.println("Eir Code: " +eir_code);
		System.out.println("Address 1: " +address1);
		System.out.println("Address 2: " +address2);
		System.out.println("Town: " +town);
		System.out.println("County: " +county);
	}
}