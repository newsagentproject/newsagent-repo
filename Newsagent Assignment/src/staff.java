import java.sql.*;
import java.util.Scanner;

public class staff 
{
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static Scanner in = new Scanner(System.in);
	public static String str;

	//For Tests
	public static int staff_id;
	public static  String firstName;
	public static String lastName;
	public static String eircode;
	public static String address1;
	public static String address2;
	public static String town;
	public static String county;
	
	
	public boolean validateStaff(int staff_id,String firstname,String lastname,String eircode,String addr1,String addr2,String town,String county)
	{
		boolean test = true;
		if(staff_id < 1000)
		{
			test = false;
		}
		if(firstname.length()>20)
		{
			test = false;
		}
		if(lastname.length()>20)
		{
			test = false;
		}
		if(eircode.length()!=8)
		{
			test = false;
		}
		if((addr1.length()>20)||(addr2.length()>20)||(town.length()>20)||(addr2.length()>20)||(county.length()>20))
		{
			test = false;
		}
		return test;
	}
	//
	
	public static void main(String[] args) 
	{
		init_db();  // open the connection to the database
		
		int selection = 0;
		final int quit = 6;
		
		while (selection != quit)
		{
			displayMainMenu(); 
			if (in.hasNextInt())
			{
				//get the menu choice from the user
				selection = in.nextInt();
				
				switch (selection)
				{
				case 1:
					displayAllStaff(); 
					break;
				case 2:
					displayParticularStaff();
					break;
				case 3:
					addNewStaff();
					break;
				case 4:
					changeStaffDetail();
					break;
				case 5: 
					deleteStaff();
					break;
				case 6:
					System.out.println("Program is closing....");
					closeConnection();
					break;
				default:
					System.out.println("You entered an invalid choice, please try again...");	
					in.nextLine();
				}
			}
			else
			{
				//Clears input and prompts for another value
				in.nextLine();
				System.out.println("You entered an invalid choice, please try again...");
			}
		}
	}
		
	public static void displayAllStaff()
	{
		String str = "Select staff_id, firstName, lastName, eircode, address1, address2, town, county from staff group by staff_id";  
			          
        try
        { 
            rs = stmt.executeQuery(str); 
            while (rs.next())
            { 
                staff_id = rs.getInt("staff_id"); 
                firstName = rs.getString("firstName"); 
                lastName = rs.getString(3); 
                eircode = rs.getString(4); 
                address1 = rs.getString(5); 
                address2 = rs.getString(6);
                town = rs.getString(7);
                county = rs.getString(8);
                System.out.print("Staff ID, FirstName, LastName, Eircode, Address1, Address2, Town, County");
                System.out.printf("%-4d%20s%30s%8s%20s%20s%20s%20s\n", staff_id, firstName, lastName, eircode, address1, address2, town, county); 
            }     
        } 
        catch (SQLException sqle) 
        { 
            System.out.println("Error: failed to display all staff."); 
            System.out.println(sqle.getMessage());
            System.out.println(str);
        }
	}
	
	public static void displayParticularStaff()
	{
		displayAllStaff(); 
		
		System.out.println("Please select the staff id of the staff member you wish to display");
		staff_id = in.nextInt();
		String str = "Select count(*) from staff where staff_id = "+staff_id;
		
		try
        { 
            rs = stmt.executeQuery(str); 
            rs.next();
            int count = rs.getInt(1);
            
            if(count < 999)
            {
            	System.out.println("Not a valid Staff ID");
            }
            else
            {
            	str = "Select * from staff where staff_id = "+staff_id;
            	rs = stmt.executeQuery(str);
            	while (rs.next()) 
                { 
                    staff_id = rs.getInt("staff_id"); 
                    firstName = rs.getString(2); 
                    lastName = rs.getString(3); 
                    eircode = rs.getString(4); 
                    address1 = rs.getString(5); 
                    address2 = rs.getString(6);
                    town = rs.getString(7);
                    county = rs.getString(8);
                    System.out.printf("%-4d%20s%30s%30s%30s%30s%30s%30s\n", staff_id, firstName, lastName, eircode, address1, address2, town, county); 
                }
            }  
        } 
        catch (SQLException sqle) 
        { 
            System.out.println("Error: Failed to display the staff member."); 
            System.out.println(sqle.getMessage());
            System.out.println(str);
        }
	}
	
	public static void addNewStaff()
	{
		System.out.println("Please enter the details about the new staff member as you are prompted for them.");
		System.out.println("Enter First Name: ");
		firstName = in.next();
		System.out.println("Enter Last Name: ");
		lastName = in.next();
		System.out.println("Enter Eircode: ");
		eircode = in.next();
		System.out.println("Enter Address 1: ");
		address1 = in.next();
		System.out.println("Enter Address 2: ");
		address2 = in.next();
		System.out.println("Enter Town: ");
		town = in.next();
		System.out.println("Enter County: ");
		county = in.next();
		str = ("INSERT INTO staff VALUES ( null, '"+firstName+"', '"+lastName+"', '"+eircode+"', '"+address1+"', '"+address2+"', '"+town+"', '"+county+"')");
		
		try
		{
			rs = stmt.executeQuery(str); 
		}
		catch (SQLException sqle) 
        { 
            System.out.println("Error: Failed to enter the new staff."); 
            System.out.println(sqle.getMessage());
            System.out.println(str);
        }		
	}
	
	public static void changeStaffDetail()
	{	
		String change; // Temporary String to hold the new value as the table updates
		
		displayAllStaff();
		System.out.println("Please select the staff id of the staff whose details you want to change");
		staff_id = in.nextInt();
		String str = "select(*) from staff where staff_id = "+staff_id;
		try
        { 
			rs = stmt.executeQuery(str); 
            rs.next();
            int count = rs.getInt(1);
            
            if(count != 0)
            {
            	System.out.printf("Please select the detail of the staff member that you wish to change");
            	System.out.printf("1. First Name");
            	System.out.printf("2. Last Name");
            	System.out.printf("3. Eircode");
            	System.out.printf("4. Address 1");
            	System.out.printf("5. Address 2");
            	System.out.printf("6. Town");
            	System.out.printf("7. County");
            	
            	int changeSelection = in.nextInt();
            	
            	if(changeSelection < 1 || changeSelection > 8)
            	{
            		System.out.println("You have not made a valid selection, please try again");
            	}
            	else
            	{
	            	int choice = rs.getInt(changeSelection);
	            	
	            	switch (choice)
					{	
					case 1:
						System.out.printf("The current First Name is"+rs.getString(2));
						System.out.printf("What would you like to change it to?");
						change = in.next();
						str = "Update staff Set firstName = '"+change+"' Where staff_id = "+staff_id;
						break;
						
					case 2:
						System.out.printf("The current Last Name is"+rs.getString(3));
						System.out.printf("What would you like to change it to?");
						change = in.next();
						str = "Update staff Set lastName = '"+change+"' Where staff_id = "+staff_id;
						break;
						
					case 3:
						System.out.printf("The current Eircode is"+rs.getString(4));
						System.out.printf("What would you like to change it to?");
						change = in.next();
						str = "Update staff Set eircode = '"+change+"' Where staff_id = "+staff_id;
						break;
						
					case 4: 
						System.out.printf("The current Address 1 is"+rs.getString(5));
						System.out.printf("What would you like to change it to?");
						change = in.nextLine();
						str = "Update staff Set address1 = '"+change+"' Where staff_id = "+staff_id;
						break;
						
					case 5:
						System.out.printf("The current Address 2 is"+rs.getString(6));
						System.out.printf("What would you like to change it to?");
						change = in.nextLine();
						str = "Update staff Set address2 = '"+change+"' Where staff_id = "+staff_id;
						break;
						
					case 6:
						System.out.printf("The current Town is"+rs.getString(7));
						System.out.printf("What would you like to change it to?");
						change = in.nextLine();
						str = "Update staff Set town = '"+change+"' Where staff_id = "+staff_id;
						break;
						
					case 7: 
						System.out.printf("The current County is"+rs.getString(8));
						System.out.printf("What would you like to change it to?");
						change = in.nextLine();
						str = "";str = "Update staff Set county = '"+change+"' Where staff_id = "+staff_id;
						break;
					default: System.out.println("You entered an invalid choice, please try again...");	
					}
            	}
            }
            else
            {
            	System.out.println("Not a valid Staff ID");
            }
        }
		catch (SQLException sqle) 
		{		
			System.out.println("Error: Failed to change the staff members detail");
			System.out.println(sqle.getMessage());
			System.out.println(str);
		}
	}
	
	public static void deleteStaff()
	{
		displayAllStaff();
		System.out.println("Please select the staff id of the staff you wish to delete");
		int staff_id_del = in.nextInt();
		String str = "Select count(*) from staff where staff_id = "+staff_id_del;
		String str2 = "Delete from staff where staff_id = "+staff_id_del;
		
		try
        { 
			rs = stmt.executeQuery(str); 
            rs.next();
            int count = rs.getInt(1);
            
            if(count == 0)
            {
            	System.out.println("Not a valid Staff ID");
            }
            else
            {
            	rs = stmt.executeQuery(str2);
            	System.out.printf("The staff member with staff id "+staff_id+" has been deleted.");
            }
        }
		catch (SQLException sqle) 
        { 
            System.out.println("Error: Failed to delete the staff member."); 
            System.out.println(sqle.getMessage());
            System.out.println(str2);
        }			
	}
	
	public static void displayMainMenu()
	{
		System.out.println("Main Menu");
		System.out.println("____________________");
		System.out.println("1: Display all staff ");
		System.out.println("2: Display a particular staff member ");
		System.out.println("3: Add a new staff member ");
		System.out.println("4: Change a detail about a staff member ");
		System.out.println("5: Delete an existing staff member ");
		System.out.println("6: Exit application");
		System.out.println("____________________");
		System.out.print("Please enter your choice: ");		
	}
	
	public static void closeConnection()
	{
		try
		{
			con.close();
			System.exit(0);
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}

	//Remember to name the datbase for the connection
	public static void init_db()
	{
		try
		{
			Class.forName("---").newInstance();
			String url="---";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
}