import junit.framework.TestCase;


public class staffTest extends TestCase 
{	
	//Test#: 001
	//Objective: Valid Staff ID
	//Input(s): 1001
	//Expected Output: True
	
	public void test001() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1001, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 002
	//Objective: Invalid Staff ID
	//Input(s): 999
	//Expected Output: False
	
	public void test002() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(999, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 003
	//Objective: Valid Staff ID
	//Input(s): 1000
	//Expected Output: True
	
	public void test003() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 004
	//Objective: Valid Staff FirstName
	//Input(s): Joe
	//Expected Output: True
	
	public void test004() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 005
	//Objective: Invalid Staff FirstName
	//Input(s): ABCDEFGHIJKLMNOPQRSTUVWXYZ (Longer than 20 chars)
	//Expected Output: False
	
	public void test005() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 006
	//Objective: Valid Staff LastName
	//Input(s): Bloggs
	//Expected Output: True
	
	public void test006() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 007
	//Objective: Invalid Staff LastName
	//Input(s): ABCDEFGHIJKLMNOPQRSTUVWXYZ (Longer than 20 chars)
	//Expected Output: False
	
	public void test007() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "Joe", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 008
	//Objective: Valid Staff Eircode
	//Input(s): N93 5h56
	//Expected Output: True	
	
	public void test008() throws InvalidStaffExceptionHandler		
	{
		staff s = new staff();		
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
		
	//Test#: 009_1
	//Objective: Invalid Staff Eircode
	//Input(s): 123456789 (Longer than 8 chars)
	//Expected Output: False
	
	public void test009_1() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "123456789", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 009_2
		//Objective: Invalid Staff Eircode
		//Input(s): 1234567 (Shorter than 8 chars)
		//Expected Output: False
		
		public void test009_2() throws InvalidStaffExceptionHandler
		{
			staff s = new staff();
			assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "1234567", "Easy Street", "Church Road", "Athlone", "Westmeath"));
		}
	
	//Test#: 010
	//Objective: Valid Staff Address 1
	//Input(s): Easy Street
	//Expected Output: True	
	
	public void test010() throws InvalidStaffExceptionHandler		
	{
		staff s = new staff();		
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
		
	//Test#: 011
	//Objective: Invalid Staff Address 1
	//Input(s): ABCDEFGHIJKLMNOPQRSTUVWXYZ
	//Expected Output: False
			
	public void test011() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 012
	//Objective: Valid Staff Address 2
	//Input(s): Church Road
	//Expected Output: True	
	
	public void test012() throws InvalidStaffExceptionHandler		
	{
		staff s = new staff();		
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
		
	//Test#: 013
	//Objective: Invalid Staff Address 2
	//Input(s): ABCDEFGHIJKLMNOPQRSTUVWXYZ
	//Expected Output: False
			
	public void test013() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "Athlone", "Westmeath"));
	}
	
	//Test#: 014
	//Objective: Valid Staff Town
	//Input(s): Athlone
	//Expected Output: True	
	
	public void test014() throws InvalidStaffExceptionHandler		
	{
		staff s = new staff();		
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
		
	//Test#: 015
	//Objective: Invalid Staff Town
	//Input(s): ABCDEFGHIJKLMNOPQRSTUVWXYZ
	//Expected Output: False
			
	public void test015() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "Westmeath"));
	}
	
	//Test#: 016
	//Objective: Valid Staff County
	//Input(s): Westmeath
	//Expected Output: True	
	
	public void test016() throws InvalidStaffExceptionHandler		
	{
		staff s = new staff();		
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
			
	//Test#: 017
	//Objective: Invalid Staff County
	//Input(s): ABCDEFGHIJKLMNOPQRSTUVWXYZ
	//Expected Output: False
	
	public void test017() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1000, "Joe", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"));
	}
	
	//Test#: 018
	//Objective: First Name Present
	//Input(s): ___
	//Expected Output: False
	
	public void test018() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1001, "", "Bloggs", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 019
	//Objective: Last Name Present
	//Input(s): ___
	//Expected Output: False
	
	public void test019() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1001, "Joe", "", "N93 5h56", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
	
	//Test#: 020
	//Objective: Eircode Present
	//Input(s): ___
	//Expected Output: False
	
	public void test020() throws InvalidStaffExceptionHandler
	{
		staff s = new staff();
		assertEquals(true,s.validateStaff(1001, "Joe", "Bloggs", "", "Easy Street", "Church Road", "Athlone", "Westmeath"));
	}
}
