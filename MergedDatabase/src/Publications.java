import java.sql.SQLException;

public class Publications {

	private int pub_id;
	private String title;
	private double  price;
	private int[] idList = new int[1];
	
	public Publications()
	{
		
	}
	public Publications(int pub_id,String title,double price)
	{
		this.pub_id = pub_id;
		this.title = title;
		this.price = price;
	}
	public boolean addPublication(int pub_id, String title, double price) throws NewsagentExceptionHandler, SQLException
	{
		NewsagentDAO n = new NewsagentDAO();
		for(int i = 0; i < idList.length; i++)
		{
			if(pub_id < 1 || price < 0)
			{
				//throw new NewsagentExceptionHandler("Publication ID invalid");
				return false;
			}
			else
			{
				idList[i] = pub_id;
				setPub_id(pub_id);
				setPrice(price);
				setTitle(title);
				//n.addPublication(pub_id, title, price);
				return true;
			}
		}
		return false;
	}
	public int getPub_id() {
		return pub_id;
	}

	public void setPub_id(int pub_id) {
		this.pub_id = pub_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public String toString()
	{
		return "Pub ID: " +pub_id+ "Title: " +title+ "Price: " +price+ "";
	}
}