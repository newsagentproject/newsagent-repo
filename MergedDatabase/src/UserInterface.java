import java.sql.SQLException;
import java.util.Scanner;


public class UserInterface
{
	NewsagentDAO database = new NewsagentDAO();
	
	public void start() throws SQLException
	{
		Scanner in = new Scanner(System.in);
		
		System.out.println("1. View All Customers");
		System.out.println("2. View All Publications");
		System.out.println("3. View All Delivery Agents");
		System.out.println("4. View All Orders");
		System.out.println("5. View All Staff");
		System.out.println("6. View All Bills");
		
		int choice = in.nextInt();
		
		while(choice != 0)
		{
			if(choice == 1)
			{
				database.getCustomers();
			}
			if(choice == 2)
			{
				//getPublications
			}
			if(choice == 3)
			{
				//getDeliveries
			}
			if(choice == 4)
			{
				//getOrders
			}
			if(choice == 5)
			{
				//getStaff
			}
			if(choice == 6)
			{
				//getBills
			}
		}
	}
}
