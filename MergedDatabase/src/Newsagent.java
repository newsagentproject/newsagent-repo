import java.sql.SQLException;
import java.util.Scanner;


public class Newsagent {

	public static void main(String[] args) throws SQLException, NewsagentExceptionHandler {
		// TODO Auto-generated method stub
		NewsagentDAO dao = new NewsagentDAO();
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("1. Display Customers");
		System.out.println("2. Display Publications");
		System.out.println("3. Display Deliveries");
		System.out.println("4. Total Orders");
		System.out.println("5. Display Staff");
		System.out.println("6. Total Bills");
		
		int choice = in.nextInt();
		
		while(choice != -1)
		{
			if(choice == 1)
			{
				dao.showCustomers();
			}
			if(choice == 2)
			{
				dao.showPublications();
			}
			if(choice == 3)
			{
				dao.showDeliveries();
			}
			if(choice == 4)
			{
				dao.getOrders();
			}
			if(choice == 5)
			{
				dao.showStaff();
			}
			if(choice == 6)
			{
				dao.getBill();
			}
			
			choice = in.nextInt();
		}
	}
}
