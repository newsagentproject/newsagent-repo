import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.Test;


public class Publication_JUnit extends TestCase {
	
	
	@Test
	//Test#1 
	//Test Objective: valid title / valid length
	//Input(s): Harry Potter I
	//Expected Output(s): true
	public void testDisplayByTitle001() 
	{
		Publication pc= new Publication();
		pc.setTitle("Harry Potter");
		assertEquals(true,pc.getTitle());
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#2 
	//Test Objective: Invalid title / Invalid title length 
	//Input(s): AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	//Expected Output(s): false
	public void testDisplayByTitle002() 
	{
		Publication pc= new Publication();
		pc.setTitle("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		assertEquals(false,pc.getTitle());
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#3 
	//Test Objective: Invalid title / Contains Nothing
	//Input(s): null
	//Expected Output(s): false
	public void testDisplayByTitle003() 
	{
		Publication pc= new Publication();
		pc.setTitle("");
		assertEquals(false,pc.getTitle());
		//fail("Not yet implemented");
	}

	@Test
	//Test#4
	//Test Objective: Invalid pub_id
	//Input(s): -1
	//Expected Output(s): false
	public void testDisplayByID001() 
	{
		Publication pc= new Publication();
		pc.setPub_id(-1);
		assertEquals(false,pc.getPub_id());
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#5
	//Test Objective: valid pub_id
	//Input(s): 1
	//Expected Output(s): true
	public void testDisplayByID002() 
	{
		Publication pc= new Publication();
		pc.setPub_id(1);
		assertEquals(true,pc.getPub_id());
		//fail("Not yet implemented");
	}
	
//////////////////////////////////////////////////////////////////////////////	
	
	@Test
	//Test#6
	//Test Objective: valid pub_id, valid title, valid price
	//Input(s): 9, harry potter, 3.80
	//Expected Output(s): true
	public void AddPublisher001() 
	{
		Publication pc= new Publication();
		pc.setPub_id(1005);
		pc.setTitle("Something New");
		pc.setPrice(10.00);
		if (pc != null)
		{
			assertThat(Publication_JUnit.AddPublisher001(pc),instanceOf(Publication.class));
		}
		assertEquals(true,pc.addPublication(9, "Harry Potter IX", 3.80));
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#7
	//Test Objective: invalid pub_id, valid title, valid price
	//Input(s): -1, harry potter, 3.80
	//Expected Output(s): false
	public void AddPublisher002() 
	{
		Publication pc= new Publication();
		assertEquals(false,pc.addPublication(-1, "Harry Potter IX", 3.80));
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#8
	//Test Objective: invalid pub_id, invalid title/length exceeded, valid price
	//Input(s): -1, harry potterAAAAAAAAAAAAAAAAAAAAAAA, 3.80
	//Expected Output(s): false
	public void AddPublisher003() 
	{
		Publication pc= new Publication();
		assertEquals(false,pc.addPublication(-1, " harry potterAAAAAAAAAAAAAAAAAAAAAAA", 3.80));
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#9
	//Test Objective: invalid pub_id, invalid title/length exceeded, invalid price
	//Input(s): -1, harry potterAAAAAAAAAAAAAAAAAAAAAAA, -1
	//Expected Output(s): false
	public void AddPublisher004() 
	{
		Publication pc= new Publication();
		assertEquals(false,pc.addPublication(-1, " harry potterAAAAAAAAAAAAAAAAAAAAAAA", -1));
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#10
	//Test Objective: valid pub_id, invalid title/ length exceeded, valid price
	//Input(s): 9,  harry potterAAAAAAAAAAAAAAAAAAAAAAA, 3.80
	//Expected Output(s): false
	public void AddPublisher005() 
	{
		Publication pc= new Publication();
		assertEquals(false,pc.addPublication(9, " harry potterAAAAAAAAAAAAAAAAAAAAAAA", 3.80));
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#11
	//Test Objective: valid pub_id, invalid title/ length exceeded, invalid price
	//Input(s): 9,  harry potterAAAAAAAAAAAAAAAAAAAAAAA, -3.80
	//Expected Output(s): false
	public void AddPublisher006() 
	{
		Publication pc= new Publication();
		assertEquals(false,pc.addPublication(9, " harry potterAAAAAAAAAAAAAAAAAAAAAAA", -3.80));
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#12
	//Test Objective: valid pub_id, valid title, invalid price
	//Input(s): 9, harry potter, -3.80
	//Expected Output(s): false
	public void AddPublisher007() 
	{
		Publication pc= new Publication();
		assertEquals(false,pc.addPublication(9, "Harry Potter IX", -3.80));
		//fail("Not yet implemented");
	}
	
	@Test
	//Test#13
	//Test Objective: invalid pub_id, valid title, invalid price
	//Input(s): -9, harry potter, -3.80
	//Expected Output(s): false
	public void AddPublisher008() 
	{
		Publication pc= new Publication();
		assertEquals(false,pc.addPublication(-9, "Harry Potter IX", -3.80));
		//fail("Not yet implemented");
	}
	
	
	
}
