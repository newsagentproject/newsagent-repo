

public class Publication {

	private String title;
	private int pub_id;
	private double price;
	
	public Publication()
	{
		this.title = title;
		this.pub_id = pub_id;
		this.price = price;
	}
		
	public int getPub_id() {
		return pub_id;
	}

	public void setPub_id(int pub_id) {
		this.pub_id = pub_id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle()
	{
		return title;
	}

	@Override
	public String toString() {
		return "PublicationClass [title=" + title + ", pub_id=" + pub_id
				+ ", price=" + price + "]";
	}
}
