
public class PublicationPrint {

	public void printPublicationDetails(int pub_id, String title, double price)
	{
		System.out.println("Publication ID: " + pub_id);
		System.out.println("Name: " + title);
		System.out.println("Price: " + price);
	}

}
