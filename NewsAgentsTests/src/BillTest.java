import junit.framework.TestCase;


public class BillTest extends TestCase
{
	
	//test number:0001
	//test objective:valid Bill Id
	//input(s) : 1
	//expected output(s):true
	
	public void testBill0001() throws InvalidBillExceptionHandler
	{
		Bill d = new Bill();
		assertEquals(true,d.DisplayBill(1,12.0,1001));
		
	}
		//test number:0002
		//test objective:invalid Bill Id
		//input(s) : -1
		//expected output(s):false
		
		public void testBill0002() throws InvalidBillExceptionHandler
		{
			Bill d = new Bill();
			assertEquals(true,d.DisplayBill(-1,12.0,1001));
		}
		//test number:0001
		//test objective:valid Bill Id
		//input(s) : MAX_VALUE
		//expected output(s):true
		
		public void testBill0003() throws InvalidBillExceptionHandler
		{
			Bill d = new Bill();
			assertEquals(true,d.DisplayBill(Integer.MAX_VALUE,12.0,1001));
			
		}
		//test number:0004
		//test objective:valid Bill Id
		//input(s) : MAX_VALUE+1
		//expected output(s):false
				
				public void testBill0004() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill((Integer.MAX_VALUE+1),12.0,1001));
					
				}
				//test number:0005
				//test objective:valid Bill Id
				//input(s) : MAX_VALUE-1
				//expected output(s):true
				
				public void testBill0005() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill((Integer.MAX_VALUE-1),12.0,1001));
					
				}
				//test number:0006
				//test objective:valid Bill Total Price
				//input(s) : 12.0
				//expected output(s):true
				
				public void testBill0006() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,12.0,1001));
					
				}
				//test number:0007
				//test objective:invalid Bill Id
				//input(s) : -1
				//expected output(s):false
				
				public void testBill0007() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,-1,1001));
					
				}
				//test number:0008
				//test objective:valid Bill Total Price
				//input(s) : MAX_VALUE DOUBLE
				//expected output(s):true
				
				public void testBill0008() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,(Double.MAX_VALUE),1001));
					
				}
				//test number:0009
				//test objective:valid Bill Total Price
				//input(s) : MAX_VALUE DOUBLE+1
				//expected output(s):false
				
				public void testBill0009() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,(Double.MAX_VALUE+1),1001));
					
				}
				//test number:0010
				//test objective:valid Bill Total Price
				//input(s) : MAX_VALUE DOUBLE-1
				//expected output(s):true
				
				public void testBill0010() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,(Double.MAX_VALUE-1),1001));
					
				}
				//test number:0011
				//test objective:valid Customer Id
				//input(s) : 1000
				//expected output(s):true
				
				public void testBill0011() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,12.0,1000));
					
				}
				//test number:0012
				//test objective:invalid Customer Id
				//input(s) : 999
				//expected output(s):false
				
				public void testBill0012() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,12.0,999));
					
				}
				//test number:0013
				//test objective:valid Customer Id
				//input(s) : MAX_VALUE Integer
				//expected output(s):true
				
				public void testBill0013() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,12.0,(Integer.MAX_VALUE)));
					
				}
				//test number:0014
				//test objective:valid Customer Id
				//input(s) : MAX_VALUE Integer+1
				//expected output(s):false
				
				public void testBill0014() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,12.0,(Integer.MAX_VALUE+1)));
					
				}
				//test number:0011
				//test objective:valid Customer Id
				//input(s) : MAX_VALUE Integer-1
				//expected output(s):true
				
				public void testBill0015() throws InvalidBillExceptionHandler
				{
					Bill d = new Bill();
					assertEquals(true,d.DisplayBill(1,12.0,(Integer.MAX_VALUE-1)));
					
				}
}

