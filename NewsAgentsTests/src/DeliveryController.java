
public class DeliveryController {

	private Delivery model;
	private DeliveryView view;
	
	 public DeliveryController(Delivery model, DeliveryView view){
	        this.model = model;
	        this.view = view;
	    }
	 
	 public void setPrice(double price)
	 {
		 model.setPrice(price);
	 }
	 
	 public double getPrice(double price)
	 {
		 return model.getPrice();
	 }
	 
	 public String getDate()
	 {
		 return model.getDate();
	 }
	 
	 public void setBillID(int bill_id)
	 {
		 model.setBill_id(bill_id);
	 }
	 
	 public int getBillID()
	 {
		 return model.getBill_id();
	 }
}
