public class Delivery
{
	private int delivery_id;
	private double price;
	private int bill_id;
	private int staff_id;
	private int day;
	private int month;
	private int year;
	private String date;
	private int lastAssigned = 0;
	
	
	public boolean addDelivery(int deli_id,double price,int day,int month, int year,int bill_id,int staff_id)
	{
		if(deli_id < 1)
		{
			return false;
		}
		else if(price < 0.01)
		{
			return false;
		}
		else if(day < 1 || day > 31)
		{
			return false;
		}
		else if(month < 1 || month > 12)
		{
			return false;
		}
		else if(year < 2016 || year > 2018)
		{
			return false;
		}
		else if(bill_id < 1)
		{
			return false;
		}
		else if(staff_id < 1)
		{
			return false;
		}
		
		return true;
	}
	public Delivery()
	{
		this.delivery_id = 10;
		this.price = 20.00;
		this.bill_id = 20;
		this.date = day + "//" + month + "//" + year;
		
		lastAssigned++;
		this.staff_id = lastAssigned;
	}
	
	public void print()
	{
		Delivery d = new Delivery();
		
		System.out.println("Delivery ID: " + d.getDelivery_id());
		System.out.println("Price: " + d.getPrice());
		System.out.println("Bill ID: " + d.getBill_id());
		System.out.println("Date: " + d.getDate());
		System.out.println("Staff ID: " + d.getStaff_id());
	}
	
	//Getters
	
	
	public double getPrice() {
		return price;
	}

	public int getBill_id() {
		return bill_id;
	}
	
	public String getDate() {
		return day + "//" + "month" + "//" + year;
	}
	
	public int getStaff_id() {
		return staff_id;
	}

	public int getLastAssigned() {
		return lastAssigned;
	}
	
	public int getDelivery_id() {
		return delivery_id;
	}
	
	//Setters
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public void setDate(String date) {
		this.date = date;
	}

	public void setBill_id(int bill_id) {
		this.bill_id = bill_id;
	}

	public void setStaff_id(int staff_id) {
		this.staff_id = staff_id;
	}

	public void setLastAssigned(int lastAssigned) {
		this.lastAssigned = lastAssigned;
	}
	
	public void setDelivery_id(int delivery_id) {
		this.delivery_id = delivery_id;
	}

}