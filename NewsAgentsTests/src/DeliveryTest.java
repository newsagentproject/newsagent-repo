import junit.framework.TestCase;


public class DeliveryTest extends TestCase {

	//Test# 001
	//Test objective: valid delivery_id
	//Input(s): 1
	//Expected output(s) true
	
	public void testaddDelivery001()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,12.00,20,2,2017,12,23));
	}
	
	//Test# 002
	//Test objective: invalid delivery_id
	//Input(s): -1
	//Expected output(s) false
	
	public void testaddDelivery002()
	{
		Delivery d = new Delivery();
		assertEquals(false,d.addDelivery(-1,12.0,20,2,2017,12,23));
	}
	
	//Test# 003
	//Test objective: valid price
	//Input(s): 0.01
	//Expected output(s) true
	
	public void testaddDelivery003()
	{
		Delivery d = new Delivery();
		assertEquals(true,d.addDelivery(1,0.01,20,2,2017,12,23));
	}
	
	//Test# 004
	//Test objective: invalid price
	//Input(s): -0.99
	//Expected output(s) false
	
	public void testaddDelivery004()
	{
		Delivery d = new Delivery();
		assertEquals(false,d.addDelivery(1,-0.99,20,2,2017,12,23));
	}
	
	//Test# 005
	//Test objective: valid bill_id
	//Input(s): 10
	//Expected output(s) true
	
	public void testaddDelivery005()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,20,2,2017,10,23));
	}
	
	//Test# 006
	//Test objective: invalid bill_id
	//Input(s): -1
	//Expected output(s) false
	
	public void testaddDelivery006()
	{
		Delivery d = new Delivery();
		assertEquals(false,d.addDelivery(1,-0.99,20,2,2017,-1,23));
	}
	
	//Test# 007
	//Test objective: valid staff_id
	//Input(s): 1000
	//Expected output(s) true
	
	public void testaddDelivery007()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,20,2,2017,10,1000));
	}
	
	//Test# 008
	//Test objective: invalid staff_id
	//Input(s): -1
	//Expected output(s) false
	
	public void testaddDelivery008()
	{
		Delivery d = new Delivery();
		assertEquals(false,d.addDelivery(1,5.00,20,2,2017,10,-1));
	}
	
	//Date Test Cases, Day
	
	//Test# 009
	//Test objective: valid day
	//Input(s): 10
	//Expected output(s) true
	
	public void testaddDelivery009()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,10,2,2017,10,1));
	}
	
	//Test# 010
	//Test objective: valid day, lowest boundary
	//Input(s): 1
	//Expected output(s) true
	
	public void testaddDelivery010()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,1,2,2017,10,1));
	}
	
	//Test# 011
	//Test objective: valid day, highest boundary
	//Input(s): 31
	//Expected output(s) true
	
	public void testaddDelivery011()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,1,2,2017,10,1));
	}
	
	//Test# 012
	//Test objective: invalid day, lowest boundary
	//Input(s): 0
	//Expected output(s) false
	
	public void testaddDelivery012()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,1,2,2017,10,1));
	}
	
	//Test# 013
	//Test objective: invalid day, highest boundary
	//Input(s): 32
	//Expected output(s) false
	
	public void testaddDelivery013()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,1,2,2017,10,1));
	}
	
	//Date Test Cases, Month
	
	//Test# 014
	//Test objective: valid month
	//Input(s): 8
	//Expected output(s) true
	
	public void testaddDelivery014()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,10,8,2017,10,1));
	}
	
	//Test# 015
	//Test objective: valid month, lowest boundary
	//Input(s): 1
	//Expected output(s) true
	
	public void testaddDelivery015()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,1,1,2017,10,1));
	}
	
	//Test# 016
	//Test objective: valid month, highest boundary
	//Input(s): 12
	//Expected output(s) true
	
	public void testaddDelivery016()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,1,12,2017,10,1));
	}
	
	//Test# 017
	//Test objective: invalid month, lowest boundary
	//Input(s): 0
	//Expected output(s) false
	
	public void testaddDelivery017()
	{
		Delivery d = new Delivery();
		assertEquals(false, d.addDelivery(1,5.00,1,0,2017,10,1));
	}
	
	//Test# 018
	//Test objective: invalid month, highest boundary
	//Input(s): 13
	//Expected output(s) false
	
	public void testaddDelivery018()
	{
		Delivery d = new Delivery();
		assertEquals(false, d.addDelivery(1,5.00,1,13,2017,10,1));
	}
	
	//Date Test Cases, Year
	
	//Test# 019
	//Test objective: valid year
	//Input(s): 2017
	//Expected output(s) true
	
	public void testaddDelivery019()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,10,8,2017,10,1));
	}
	
	//Test# 020
	//Test objective: valid year, lowest boundary
	//Input(s): 2016
	//Expected output(s) true
	
	public void testaddDelivery020()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,1,1,2017,10,1));
	}
	
	//Test# 021
	//Test objective: valid year, highest boundary
	//Input(s): 2018
	//Expected output(s) true
	
	public void testaddDelivery021()
	{
		Delivery d = new Delivery();
		assertTrue(d.addDelivery(1,5.00,1,12,2017,10,1));
	}
	
	//Test# 022
	//Test objective: invalid year, lowest boundary
	//Input(s): 2015
	//Expected output(s) false
	
	public void testaddDelivery022()
	{
		Delivery d = new Delivery();
		assertEquals(false, d.addDelivery(1,5.00,1,0,2017,10,1));
	}
	
	//Test# 023
	//Test objective: invalid year, highest boundary
	//Input(s): 2019
	//Expected output(s) false
	
	public void testaddDelivery023()
	{
		Delivery d = new Delivery();
		assertEquals(false, d.addDelivery(1,5.00,1,13,2017,10,1));
	}
}
