
public class Staff {

	private int staff_id;
	private String firstName;
	private String lastName;
	private int eir_code;
	private String address1;
	private String address2;
	private String town;
	private String county;
	
	public Staff(int staff_id,String firstName,String lastName,int eir_code,String address1,String address2,String town,String county)
	{
		this.staff_id = staff_id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.eir_code = eir_code;
		this.address1 = address1;
		this.address2 = address2;
		this.town = town;
		this.county = county;
	}

	public int getStaff_id() {
		return staff_id;
	}

	public void setStaff_id(int staff_id) {
		this.staff_id = staff_id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getEir_code() {
		return eir_code;
	}

	public void setEir_code(int eir_code) {
		this.eir_code = eir_code;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	public String toString()
	{
	return "Staff ID: " +staff_id+ "First Name: " +firstName+ "Last Name: " +lastName+ "Eir Code: " +eir_code+ "Address 1: " +address1+ "Address 2: " +address2+ "Town: " +town+ "County: " +county+ "";	
	}
}
