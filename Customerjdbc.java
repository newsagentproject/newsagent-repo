import java.sql.PreparedStatement;

public class Customerjdbc  extends NewsagentDAO
{

	public static void addCustomer(Customer C)
	{
		try{
			
			String str = "Insert into Customer Values(null,12537,joe,johnson,7,aptartment street,tullamore,Offaly)";
			PreparedStatement pstmt = con.prepareStatement(str);
			
			
			pstmt.setInt(1, C.getCus_id());
			pstmt.setInt(1, C.getEir_Code());
			pstmt.setString(1, C.getFirstName());
			pstmt.setString(1, C.getLastName());
			pstmt.setString(1, C.getAddress1());
			pstmt.setString(1, C.getAddress2());
			pstmt.setString(1, C.getTown());
			pstmt.setString(1, C.getCounty());
			
			int num=pstmt.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println("Error adding Customer");
			e.printStackTrace();
		}
		
	}
	public static void displayCustomer(Customer C) 
	{
	          
        try
        { 
           
                System.out.println("Customer Id: "+C.getCus_id());
                System.out.println("Customer Firstname: "+C.getFirstName());
                System.out.println("Customer Secondname: "+C.getLastName());
                System.out.println("Customer Address line 1: "+C.getAddress1());
                System.out.println("Customer Address line 2: "+C.getAddress2());
                System.out.println("Customer Town: "+C.getTown());
                System.out.println("Customer County: "+C.getCounty());
                System.out.println("Customer Eir-Code: "+C.getEir_Code());
              
        } 
        catch (Exception e) 
        { 
        	System.out.println("Error displaying Customer");
			e.printStackTrace();
        }
	}
	public static void main(String [] args)
	{
		Customer c = new Customer();
		displayCustomer(c);
	}
}
